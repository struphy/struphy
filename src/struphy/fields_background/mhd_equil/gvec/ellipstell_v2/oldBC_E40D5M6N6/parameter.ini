!================================================================================================================================= !
! PARAMETERFILE 
! execute with
! > ../../build/bin/gvec parameter.ini

!================================================================================================================================= !
! running tests at startup 
!================================================================================================================================= !
  testlevel =-1 ! different levels for testing. -1: off, >0: run tests
    testdbg = F !T: set all tests to failed (to debug tests), F: only failed tests are printed

!================================================================================================================================= !
! compute initial solution from existing equilibrium
!================================================================================================================================= !

  ProjectName = GVEC_ELLIPSTELL_V2


  whichInitEquilibrium = 0 ! 0: only from input parameters, 1: from VMEC netcdf file, 2: from internally generated Soloview

!    vmecwoutFile= wout_R4ellipStell_nfp2_n1_iota09_-03_p1_-01_-08.nc
!    vmecwoutFile= wout_R4ellipStell_nfp2_n1_iota09_p0.nc
!    vmecwoutFile= wout_R4ellipStell_nfp2_n3_iota09-06_p0.nc 

!    init_fromBConly = F ! ONLY used if whichInitEquilibrium=1 (VMEC input)
                        ! =T: only use axis and boundary for X1,X2 (True is default) 
                        ! =F: if VMEC input is used, interpolate VMEC data  on the full mesh s=0...1

  init_LA         = T ! T: compute lambda from initial mapping (default),  
                      ! F: lambda=0 or if whichInitEquilibrium=1 use VMEC at initialization 


!================================================================================================================================= !
! visualization parameters
!================================================================================================================================= !
which_visu = 0          ! 0: visualize gvec data, 1: visualize vmec data (whichInitEquilibrium=1)
visu1D       = 12     ! 0: off, >0:  write 1D data of all modes of R,Z,lambda over the flux / radial coordinate
  np_1d      = 5 
visu2D       = 12       ! 0: off, 1: write paraview file of boundary, 2: write paraview file for zeta=const. planes 
  np_visu_BC     =   (/80,80/)
  visu_BC_min     = (/ 0.,0. /)  ! range of visualization for s,theta,zeta: min in [0,1]
  visu_BC_max     = (/ 1.,1. /)  !                                          max in [0,1]
  np_visu_planes = (/3,40,5/)

  visu_planes_min     = (/0. ,-0.5 ,0. /)  ! range of visualization for s,theta,zeta: min in [0,1]
  visu_planes_max     = (/1. , 0.5 ,0.5 /)  !                                          max in [0,1]
visu3D       = 0 !1        ! 0: off, 1: write 3D paraview file 
!  np_visu_3D     = (/2,120,360/)
  np_visu_3D     = (/2,30,40/)

!visu_min/max applies to visu_BC,visu_planes and visu_3D if not specified
visu_3D_min     = (/0. ,0. ,0. /)  ! range of visualization for s,theta,zeta: min in [0,1]
visu_3D_max     = (/1. ,1. ,0.5 /)  !                                          max in [0,1]

SFL_theta= F
!================================================================================================================================= !
! grid in s direction
!================================================================================================================================= !
sgrid_nElems=20
sgrid_grid_type=4  ! 0: uniform, 1: sqrt (finer at edge), 2: s^2 (finer at center), 3: bump (fine at edge and center)

!================================================================================================================================= !
! discretization parameters
!================================================================================================================================= !

degGP  = 7               ! number of gauss points per radial element

fac_nyq = 4             ! number of points for integration in (theta,zeta), :mn_nyq=fac_nyq * max(mn_max (all variables)) 
 
X1X2_deg  = 5            !polynomial degree in radial discretization for X1 and X2 variable

nfp = 2
X1_mn_max = (/ 6, 6/)   !maximum mumber of fourier modes (mmax,nmax)
X1_sin_cos = _cos_       !which fourier modes: can be either _sin_,_cos_, or _sincos_

X2_mn_max = (/ 6, 6/)   !maximum mumber of fourier modes (mmax,nmax)
X2_sin_cos = _sin_       !

LA_deg    = 5            ! polynomial degree in radial discretization for Lambda variable
LA_continuity =4         ! continuity of the radial discretization (-1: discontinuous, deg-1: spline (default)) 
LA_mn_max = (/ 6, 6/)   ! maximum mumber of fourier modes (mmax,nmax)
LA_sin_cos = _sin_       !

which_hmap = 1           ! type of global mapping h between (X1,X1,zeta)->(x,y,z), 1: R=X1,Z=X2 torus, 2: cylinder, 10: knot
  hmap_cyl_len=20.       ! for cylinder map


!================================================================================================================================= !
! fourier modes at the edge boundary condition
! FORMAT AA_X_ccc( M; N) with AA: X1,X2 , X=a/b (axis/edge) 
!                             ccc: cos/sin, ( M; N) without NFP with any number of whitespaces: (  0;  0) ( 1; 0) ( -1; -2) 
! modes which are not specified here are set to zero!!!
!================================================================================================================================= !
X1_b_cos(  0;  0) =  4.0 

X1_b_cos(  1;  0) =  1.0 
X2_b_sin(  1;  0) =  1.0

X1_b_cos(  1; -1) =  0.3 
X2_b_sin(  1; -1) = -0.3 

X2_b_sin(  0;  1) = -0.2

!axis only used for initialization:
X1_a_cos(  0;  0) = 4.003

X1_a_cos(  0;  1) = 0.0 
X2_a_sin(  0;  1) = 0.0 


!================================================================================================================================= !
! profiles
!================================================================================================================================= !
iota_coefs= (/  0.9,-0.1 /) !(a,b,c,d,...) : iota(phi_norm)=a+b*phi_norm+c*phi_norm^2 ...             phi_norm: normalized tor.flux
pres_coefs= (/ 1.0, -0.1,-0.8 /)
pres_scale=1500. !

!iota_sign = -1
!iota_coefs= (/ 0.9, 0.0 /) !(a,b,c) : iota(phi_norm)=a+b*phi_norm+c*phi_norm^2 ...             phi_norm: normalized tor.flux
!pres_coefs= (/ 0.0, 0. /)

PHIEDGE    = -1.5                ! torodial flux at the last flux surface


!================================================================================================================================= !
! iteration
!================================================================================================================================= !
MaxIter =400000            ! maximum number of iterations
start_dt=5.0e-1           ! first timestep, is automatically adapted
minimize_tol=1.0e-10   ! absolute tolerance |Force|*dt<abstol
outputIter = 20000          ! number of iterations after which a state & visu is written
logIter = 1000             ! write log of iterations to screen 

PrecondType =1 ! -1: off(default), 1: on
MinimizerType = 0 ! 0: descent(default), 10: accelerated GD
