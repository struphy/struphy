grid :
    Nel      : [1, 1, 64] # number of grid cells, >p
    p        : [1, 1, 3]  # spline degree
    spl_kind : [True, True, True] # spline type: True=periodic, False=clamped
    dirichlet_bc : null # [[False, False], [False, False], [False, False]], hom. Dirichlet boundary conditions for N-splines (spl_kind must be False)
    dims_mask : [True, True, True] # True if the dimension is to be used in the mpi domain decomposition (=default for each dimension).
    nq_el    : [1, 1, 6] # quadrature points per grid cell
    nq_pr    : [1, 1, 4] # quadrature points per histopolation cell (for commuting projectors)
    polar_ck : -1 # C^k smoothness at polar singularity at eta_1=0 (default: -1 --> standard tensor product, 1 : polar splines)

units :
    x : 1. # length scale unit in m
    B : 1. # magnetic field unit in T
    n : 1. # number density unit of fluid species in 1 x 10^20 m^(-3)

time :
    dt         : 0.15 # time step
    Tend       : 180. # simulation time interval is [0, Tend]
    split_algo : LieTrotter # LieTrotter | Strang

geometry :
    type : Cuboid # mapping F (possible types seen below)
    Cuboid :
        l1 : 0. # start of interval in eta1
        r1 : 1. # end of interval in eta1, r1>l1
        l2 : 0. # start of interval in eta2
        r2 : 1. # end of interval in eta2, r2>l2
        l3 : 0. # start of interval in eta3
        r3 : 60. # end of interval in eta3, r3>l3

mhd_equilibrium :
    type : HomogenSlab # (possible choices seen below)
    HomogenSlab :
        B0x  : 0. # magnetic field in x
        B0y  : 1. # magnetic field in y
        B0z  : 1. # magnetic field in z
        beta : 1. # plasma beta = 2*p*mu_0/B^2
        n0   : 1. # number density

em_fields:
    background:
        type : MHD
        MHD :
            comps :
                b2 : b2

    save_data :
        comps :
            b2 : true
    options:
        VariationalMagFieldEvolve:
            lin_solver:
                type: [pcg, MassMatrixDiagonalPreconditioner]
                tol: 1.e-12
                maxiter: 500
                non_linear_maxiter: 100
                verbose: False
                recycle: True
            nonlin_solver:
                type: Newton
                tol: 1.e-8
                maxiter: 100
                info: False
                implicit_transport: false

fluid :
    mhd :
        phys_params:
            A : 1  # mass number in units of proton mass
            Z : 1 # signed charge number in units of elementary charge
        mhd_u_space: H1vec
        background:
            type : MHD
            MHD :
                comps :
                    rho3 : n3
                    s3   : s3_monoatomic
        perturbation :
            type : noise # initialization
            noise :
                comps :
                    rho3 : False              # components to be initialized (for scalar fields: no list)
                    uv   : [True, True, True] # components to be initialized (for scalar fields: no list)
                    s3   : False              # components to be initialized (for scalar fields: no list)
                direction : e3 # noise variation (logical space): e1, e2, e3 (1d), e1e2, e1e3, e2e3 (2d), e1e2e3 (3d)
                amp : 0.01 # noise amplitude
                seed : 1234    # seed for random number generator
        options:
            VariationalMomentumAdvection:
                lin_solver:
                    type: [pcg, MassMatrixDiagonalPreconditioner]
                    tol: 1.e-12
                    maxiter: 500
                    verbose: False
                    recycle: True
                nonlin_solver:
                    type: Newton
                    tol: 1.e-8
                    maxiter: 100
                    info: False
            VariationalDensityEvolve:
                lin_solver:
                    type: [pcg, MassMatrixDiagonalPreconditioner]
                    tol: 1.e-12
                    maxiter: 500
                    verbose: False
                    recycle: True
                nonlin_solver:
                    type: Newton
                    tol: 1.e-8
                    maxiter: 100
                    info: False
                    implicit_transport: false
                physics: 
                    gamma: 1.6666666666666667
            VariationalEntropyEvolve:
                lin_solver:
                    type: [pcg, MassMatrixDiagonalPreconditioner]
                    tol: 1.e-12
                    maxiter: 500
                    verbose: False
                    recycle: True
                nonlin_solver:
                    type: Newton
                    tol: 1.e-8
                    maxiter: 100
                    info: False
                    implicit_transport: false
                physics: {gamma: 1.6666666666666667}
            
kinetic: {}
