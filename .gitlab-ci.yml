# Keyword reference for the .gitlab-ci.yml file: 
# https://gitlab.mpcdf.mpg.de/help/ci/yaml/index.md 

# Struphy test strategy:
# 
# The startup stage with "ci_vars" and "default_venv" is always executed.
#
# At each "push": 
# 1) build .whl
# 2) install from .whl in default and ubuntu:latest 
# 3) compile only in C
# 4) perform all tests (including models --fast) in default and ubuntu:latest
# 
# At each merge into "devel": 
# 1) do nothing
#
# At each merge into "master":
# 1) build .whl
# 2) install from .whl in default 
# 3) update pages 
# 4) deploy to PyPI
# 5) release on GitLab
#
# At "scheduled" pipeline:
# 1) install from PyPI in all available docker images and in default
# 2) compile in C and Fortran
# 3) run all tests (including models --fast) in all available images and in default
#

# A "scheduled" pipeline can be tested by clicking "Run pipeline" on Gitlab and setting the TEST_SCHEDULED = true.

# The doc can be built by clicking "Run pipeline" on Gitlab and setting the MAKE_PAGES = true.

variables:
  MAKE_PAGES:
    value: "false"
    options: ["true", "false"]
    description: "''true'' to make pages"
  TEST_SCHEDULED:
    value: "false"
    options: ["true", "false"]
    description: "''true'' to run the scheduled pipeline"
  STRUPHY_SOURCE_DOCKER:
    value: "dist/struphy*.whl"
    options: ["dist/struphy*.whl", "-U struphy"]
    description: "Source to install struphy from in the docker jobs."

default:
  image: gitlab-registry.mpcdf.mpg.de/mpcdf/ci-module-image/gcc_12-openmpi_4_1

  before_script:
    - date
    - ls -a
    - uname -a
    - module list
    - module avail
    - module load gcc/12 openmpi/4 anaconda/3/2023.03 git graphviz/8
    - module list
    - make -v
    - python3 --version
    - echo env_$CI_PIPELINE_ID
    - ls env_$CI_PIPELINE_ID/bin/
    - source env_$CI_PIPELINE_ID/bin/activate
    - pip list

  # tags:
  #   - distributedcache

  artifacts:
    expire_in: 1 day

stages:
  - startup
  - build
  - install
  - test
  - lint
  - pages
  - release 

# python virtual environments into cache
default_venv:
  stage: startup 
  before_script:
    - date
    - ls -a
    - uname -s # get system info
    - uname -v
    - uname -m
    - uname -o
    - echo "Hello ${GITLAB_USER_NAME} !"
    - echo "This is job ${CI_JOB_ID}"
    - module list
    - module avail
    - module load gcc/12 openmpi/4 anaconda/3/2023.03 git graphviz/8
    - module list
  script:
    - pip install -U virtualenv
    - python3 -m venv env_$CI_PIPELINE_ID
    - ls
    - pwd
    - source env_$CI_PIPELINE_ID/bin/activate
    - python3 -m pip install --upgrade pip
    - pip install -U pytest coverage twine
    - pip list
  artifacts:
    name: 'python-env'
    paths:
      - env_$CI_PIPELINE_ID/
    expire_in: 1 day

# Print some predefined variables: https://gitlab.mpcdf.mpg.de/help/ci/variables/predefined_variables.md
ci_vars:
  stage: startup
  before_script:
    - date
    - ls -a
    - module list
  script:
    - echo "All environment variables:"
    - env

# build on mpcdf server
default_build:
  stage: build
  rules:
    - if: $CI_COMMIT_TAG
      when: never
    - if: $CI_PIPELINE_SOURCE == "push"
    - if: $TEST_SCHEDULED == "true" || $CI_PIPELINE_SOURCE == "schedule"
    - if: $MAKE_PAGES == "true"
  needs: ['default_venv']
  script:  
    - pip install -U build
    - pip list
    - python3 -m build # build struphy (.whl) 
  artifacts:
    name: 'dist'
    paths:
      - dist/
    expire_in: 1 day

#####################
### INSTALL STAGE ###
#####################

# language c 
default_install:
  stage: install
  rules:
    - if: $CI_COMMIT_TAG
      when: never
    - if: $MAKE_PAGES == "true"
      when: never
    - if: $TEST_SCHEDULED == "true" || $CI_PIPELINE_SOURCE == "schedule"
    - if: $CI_COMMIT_REF_PROTECTED == "false" && $CI_PIPELINE_SOURCE == "push"
  script:
    - pip install $(echo dist/struphy*.whl)[test]
    - pip show struphy
    - pip list
    - struphy
    - pyccel --version
    - struphy compile --status
    - struphy compile --time-execution -y # compiles struphy, psydac with language c 
    - struphy --refresh-models
  artifacts:
    name: 'python-env-installed'
    paths:
      - env_$CI_PIPELINE_ID/
    expire_in: 1 day

# Cron-job C:
c_cronjob_install:
  stage: install
  rules:
    - if: $TEST_SCHEDULED == "true"
    - if: $CI_PIPELINE_SOURCE == "schedule"
  needs: ["default_install"]
  script:
    - pip install -U struphy[test]
    - pip show struphy
    - pip list
    - struphy
    - pyccel --version
    - struphy compile --status
    - struphy compile --time-execution -y # compiles struphy, psydac with language c 
  artifacts:
    name: 'python-env-installed'
    paths:
      - env_$CI_PIPELINE_ID/
    expire_in: 1 day

##################
### LINT STAGE ###
##################

# Run in weekly CI pipeline only
lint_full_repo_report:
  image: gitlab-registry.mpcdf.mpg.de/struphy/struphy/struphy_ubuntu_latest
  stage: lint
  rules:
    # Only run full lint report with pylint and flake8 on the scheduled CI pipeline
    - if: $TEST_SCHEDULED == "true"
    - if: $CI_PIPELINE_SOURCE == "schedule"
  before_script:
    - date
    - python3 -m venv env_struphy_lint
  script:
    - source env_struphy_lint/bin/activate # Temporary env for installing struphy in editable mode
    - pip install --upgrade pip
    - pip list
    - ls
    - pwd
    - pip install -e .[dev] # We have to install struphy in editable mode for struphy lint to work
    - struphy
    - struphy lint all --output-format report
    - mkdir -p lint_reports
    - mv *.html lint_reports/ 2>/dev/null || echo "No HTML files found."
  allow_failure: true
  artifacts:
    paths:
      - lint_reports/
    expire_in: 1 week

# Lint struphy with `struphy lint`
lint_branch:
  image: gitlab-registry.mpcdf.mpg.de/struphy/struphy/struphy_ubuntu_latest
  stage: lint
  rules:
    - if: $TEST_SCHEDULED == "true"
      when: never
    - if: $MAKE_PAGES == "true"
      when: never
    - if: $CI_PIPELINE_SOURCE == "schedule"
      when: never
    - if: $CI_COMMIT_TAG
      when: never
    - if: $CI_COMMIT_REF_PROTECTED == "false" && $CI_PIPELINE_SOURCE == "push"
  before_script:
    - date
    - python3 -m venv env_struphy_lint
  script:
    - source env_struphy_lint/bin/activate # Temporary env for installing struphy in editable mode
    - pip install --upgrade pip
    - pip list
    - ls
    - pwd
    - pip install -e .[dev] # We have to install struphy in editable mode for struphy lint to work
    - struphy
    # Lint changed files in current branch, FAIL the CI pipeline if any files are incorrectly formatted
    - struphy lint branch --output-format plain --verbose
  allow_failure: true

# Lint struphy with `struphy lint`
lint_branch_report:
  image: gitlab-registry.mpcdf.mpg.de/struphy/struphy/struphy_ubuntu_latest
  stage: lint
  rules:
    - if: $TEST_SCHEDULED == "true"
      when: never
    - if: $MAKE_PAGES == "true"
      when: never
    - if: $CI_PIPELINE_SOURCE == "schedule"
      when: never
    - if: $CI_COMMIT_TAG
      when: never
    - if: $CI_COMMIT_REF_PROTECTED == "false" && $CI_PIPELINE_SOURCE == "push"
  before_script:
    - date
    - python3 -m venv env_struphy_lint
  script:
    - source env_struphy_lint/bin/activate # Temporary env for installing struphy in editable mode
    - pip install --upgrade pip
    - pip list
    - ls
    - pwd
    - pip install -e .[dev] # We have to install struphy in editable mode for struphy lint to work
    - struphy
    - struphy lint branch --output-format report
    - mkdir -p lint_reports
    - mv *.html lint_reports/ 2>/dev/null || echo "No HTML files found."
  allow_failure: true
  artifacts:
    paths:
      - lint_reports/
    expire_in: 1 week

##################
### TEST STAGE ###
##################

# ubuntu latest 
Ubuntu_latest:
  stage: test
  image: ubuntu:latest
  rules:
    - if: $TEST_SCHEDULED == "true"
      when: never
    - if: $MAKE_PAGES == "true"
      when: never
    - if: $CI_PIPELINE_SOURCE == "schedule"
      when: never
    - if: $CI_COMMIT_TAG
      when: never
    - if: $CI_COMMIT_REF_PROTECTED == "false" && $CI_PIPELINE_SOURCE == "push"
  needs: ['default_build']
  before_script:
    - date
    - ls -a
    - apt update -y && apt clean 
    - export DEBIAN_FRONTEND=noninteractive
    - apt install -y software-properties-common
    - add-apt-repository -y ppa:deadsnakes/ppa
    - apt update -y
    # - apt install -y python3.11
    # - apt install -y python3.11-dev
    - apt install -y python3-pip 
    - apt install -y python3-venv 
    - apt install -y gfortran gcc 
    - apt install -y liblapack-dev libopenmpi-dev 
    - apt install -y libblas-dev openmpi-bin 
    - apt install -y libomp-dev libomp5 
    - apt install -y git
    - apt install -y pandoc
    - apt install -y sqlite3
    - export OMPI_ALLOW_RUN_AS_ROOT=1
    - export OMPI_ALLOW_RUN_AS_ROOT_CONFIRM=1
  script:
    - python3 -m pip list
    - python3 -m venv env
    - source env/bin/activate
    - python3 --version
    - pip install $(echo dist/struphy*.whl)[test]
    - struphy compile --status
    - make -v
    - struphy compile --time-execution
    - struphy -h # this is the Quickstart
    - struphy compile --status
    - struphy -p
    - struphy --set-i .
    - struphy --set-o . 
    - struphy run -h
    - struphy params VlasovMaxwellOneSpecies -y
    - ls -1a
    - mv params_VlasovMaxwellOneSpecies.yml test.yml
    - struphy run VlasovMaxwellOneSpecies -i test.yml -o my_first_sim
    - ls my_first_sim/
    - struphy pproc -d my_first_sim
    - ls my_first_sim/post_processing/fields_data/
    - ls my_first_sim/post_processing/kinetic_data/
    - struphy params VlasovMaxwellOneSpecies --options
    - struphy test models --fast
    - struphy test models --fast --verification --mpi 1
    - struphy test models --fast --verification --mpi 4

# Quickstart test
quickstart_tests:
  stage: test
  rules:
    - if: $TEST_SCHEDULED == "true"
      when: never
    - if: $MAKE_PAGES == "true"
      when: never
    - if: $CI_PIPELINE_SOURCE == "schedule"
      when: never
    - if: $CI_COMMIT_TAG
      when: never
    - if: $CI_COMMIT_REF_PROTECTED == "false" && $CI_PIPELINE_SOURCE == "push"
  script:
    - pwd
    - ls -1a
    - struphy -h # this is the Quickstart
    - struphy compile --time-execution 
    - struphy -p
    - struphy --set-i .
    - struphy --set-o . 
    - struphy run -h
    - struphy params VlasovMaxwellOneSpecies -y
    - ls -1a
    - mv params_VlasovMaxwellOneSpecies.yml test.yml
    - struphy run VlasovMaxwellOneSpecies -i test.yml -o my_first_sim
    - ls my_first_sim/
    - struphy pproc -d my_first_sim
    - ls my_first_sim/post_processing/fields_data/
    - ls my_first_sim/post_processing/kinetic_data/
    - struphy params VlasovMaxwellOneSpecies --options

    #- ls $(python3 -c "import struphy as _; print(_.__path__[0])")/_pymon --all

# default tests
c_unit_tests:
  stage: test
  rules:
    - if: $TEST_SCHEDULED == "true"
      when: never
    - if: $MAKE_PAGES == "true"
      when: never
    - if: $CI_PIPELINE_SOURCE == "schedule"
      when: never
    - if: $CI_COMMIT_TAG
      when: never
    - if: $CI_COMMIT_REF_PROTECTED == "false" && $CI_PIPELINE_SOURCE == "push"
  script:
    - pwd
    - ls -1a
    - struphy compile --status
    - struphy --refresh-models
    - struphy test unit  
    #- ls $(python3 -c "import struphy as _; print(_.__path__[0])")/_pymon --all

c_model_tests:
  stage: test
  rules:
    - if: $TEST_SCHEDULED == "true"
      when: never
    - if: $MAKE_PAGES == "true"
      when: never
    - if: $CI_PIPELINE_SOURCE == "schedule"
      when: never
    - if: $CI_COMMIT_TAG
      when: never
    - if: $CI_COMMIT_REF_PROTECTED == "false" && $CI_PIPELINE_SOURCE == "push"
  script:
    - pwd
    - ls -1a
    - struphy compile --status
    - struphy test models --fast
    - struphy test models --fast --verification --mpi 1
    - struphy test models --fast --verification --mpi 4

c_model_tests_psydac_whl:
  stage: test
  rules:
    - if: $TEST_SCHEDULED == "true"
      when: never
    - if: $MAKE_PAGES == "true"
      when: never
    - if: $CI_PIPELINE_SOURCE == "schedule"
      when: never
    - if: $CI_COMMIT_TAG
      when: never
    - if: $CI_COMMIT_REF_PROTECTED == "false" && $CI_PIPELINE_SOURCE == "push"
  script:
    - pwd
    - ls -1a
    - struphy compile --status
    - pip uninstall -y psydac
    - pip install src/struphy/psydac*.whl
    - struphy compile --time-execution
    - struphy test models --fast
    - struphy test models --fast --verification --mpi 1
    - struphy test models --fast --verification --mpi 4

pages_tests:
  stage: test
  rules:
    - if: $TEST_SCHEDULED == "true"
      when: never
    - if: $MAKE_PAGES == "true"
      when: never
    - if: $CI_PIPELINE_SOURCE == "schedule"
      when: never
    - if: $CI_COMMIT_TAG
      when: never
    - if: $CI_COMMIT_REF_PROTECTED == "false" && $CI_PIPELINE_SOURCE == "push"
  script:
    - pwd
    - ls -1a
    - module list
    - module avail
    - module load pandoc
    - module list
    - pip install $(echo dist/struphy*.whl)[doc]
    - struphy compile --time-execution -y
    - cd doc ; make html; cd ..

c_model_cronjob_tests:
  stage: test
  rules:
    - if: $TEST_SCHEDULED == "true"
    - if: $CI_PIPELINE_SOURCE == "schedule"
  script:
    - pwd
    - ls -1a
    - struphy compile --status
    - struphy test models --fast
    - struphy test models --fast --verification --mpi 1
    - struphy test models --fast --verification --mpi 4
  artifacts:
    name: 'python-env-installed-2'
    paths:
      - env_$CI_PIPELINE_ID/
    expire_in: 1 day

# Cron-job Fortran install:
fortran_cronjob_install:
  stage: test
  needs: ["c_model_cronjob_tests"]
  rules:
    - if: $TEST_SCHEDULED == "true"
    - if: $CI_PIPELINE_SOURCE == "schedule"
  script:
    - pip show struphy
    - pip list
    - struphy
    - pyccel --version
    - struphy compile --status
    - struphy compile --time-execution --language=fortran -y # compiles struphy, psydac with language fortran
  artifacts:
    name: 'python-env-installed-3'
    paths:
      - env_$CI_PIPELINE_ID/
    expire_in: 1 day

fortran_unit_tests:
  stage: test
  needs: ["fortran_cronjob_install"]
  rules:
    - if: $TEST_SCHEDULED == "true"
    - if: $CI_PIPELINE_SOURCE == "schedule"
  script:
    - pwd
    - ls -1a
    - struphy compile --status
    - struphy test unit 
    #- ls $(python3 -c "import struphy as _; print(_.__path__[0])")/_pymon --all

fortran_model_tests:
  stage: test
  needs: ["fortran_cronjob_install"]
  rules:
    - if: $TEST_SCHEDULED == "true"
    - if: $CI_PIPELINE_SOURCE == "schedule"
  script:
    - pwd
    - ls -1a
    - struphy compile --status
    - struphy test models --fast
    - struphy test models --fast --verification --mpi 1
    - struphy test models --fast --verification --mpi 4

# docker image testing (scheduled: struphy from PyPI OR test-schedule: struphy from devel)
Ubuntu_22_04:
  stage: test
  image: ubuntu:22.04
  rules:
    - if: $CI_PIPELINE_SOURCE == "schedule"
    - if: $TEST_SCHEDULED == "true"
  needs: ['default_build']
  before_script:
    - date
    - ls -a
    - apt update -y && apt clean 
    - apt install -y python3-pip 
    - apt install -y python3-venv
    - apt install -y gfortran gcc 
    - DEBIAN_FRONTEND=noninteractive TZ="Europe/Berlin" apt-get install -y liblapack-dev libopenmpi-dev 
    - apt install -y libblas-dev openmpi-bin 
    - apt install -y libomp-dev libomp5 
    - apt install -y git
    - apt install -y pandoc
    - export OMPI_ALLOW_RUN_AS_ROOT=1
    - export OMPI_ALLOW_RUN_AS_ROOT_CONFIRM=1
  script:
    - python3 -m venv env
    - source env/bin/activate
    - pip install --upgrade pip
    - echo $CI_COMMIT_MESSAGE
    - echo $TEST_SCHEDULED
    - pip install --no-cache-dir $(echo $STRUPHY_SOURCE_DOCKER)[test]
    - struphy compile --time-execution
    - struphy test models --fast
    - struphy compile --time-execution --language=fortran -y 
    - struphy test models --fast

struphy_ubuntu_python_3_11:
  stage: test
  image: gitlab-registry.mpcdf.mpg.de/struphy/struphy/struphy_ubuntu_python_3_11
  rules:
    - if: $CI_PIPELINE_SOURCE == "schedule"
    - if: $TEST_SCHEDULED == "true"
  needs: ['default_build']
  before_script:
    - date
    - uname -a
    - ls -a
    - ls dist/
    - ls /
  script:
    - python3 -V
    - python3.11 -V
    - python3.11 -m venv env
    - source env/bin/activate
    - pip install --upgrade pip
    - echo $CI_COMMIT_MESSAGE
    - echo $TEST_SCHEDULED
    - pip install --no-cache-dir $(echo $STRUPHY_SOURCE_DOCKER)[test]
    - struphy compile --time-execution
    - struphy test models --fast
    - struphy compile --time-execution --language=fortran -y
    - struphy test models --fast

struphy_ubuntu_latest:
  stage: test
  image: gitlab-registry.mpcdf.mpg.de/struphy/struphy/struphy_ubuntu_latest
  rules:
    # Temporary: Since this docker image uses python3.12, pip install -U struphy will always install 1.9.9,
    # because it is the highest version that does not have the requirement Python<3.12. Therefore, we will
    # skip this job when the scheduled pipeline is launched with the variable $STRUPHY_SOURCE_DOCKER == "-U struphy"
    - if: $STRUPHY_SOURCE_DOCKER == "-U struphy"
      when: never
    - if: $CI_PIPELINE_SOURCE == "schedule"
    - if: $TEST_SCHEDULED == "true"
  needs: ['default_build']
  before_script:
    - date
    - uname -a
    - ls -a
    - ls dist/
    - ls /
  script:
    - python3 -V
    - python3 -m venv env
    - source env/bin/activate
    - pip install --upgrade pip
    - echo $CI_COMMIT_MESSAGE
    - echo $TEST_SCHEDULED
    - pip install --no-cache-dir $(echo $STRUPHY_SOURCE_DOCKER)[test]
    - struphy compile --time-execution
    - struphy test models --fast
    - struphy compile --time-execution --language=fortran -y
    - struphy test models --fast

Fedora:
  stage: test
  image: fedora:latest
  rules:
    - if: $CI_PIPELINE_SOURCE == "schedule"
    - if: $TEST_SCHEDULED == "true"
  needs: ['default_build']
  before_script:
    - date
    - ls -a
    - dnf install -y wget yum-utils make openssl-devel bzip2-devel libffi-devel zlib-devel
    - dnf update -y  
    - dnf install -y gcc
    - dnf install -y gfortran  
    - dnf install -y blas-devel lapack-devel  
    - dnf install -y openmpi openmpi-devel 
    - dnf install -y libgomp 
    - dnf install -y git 
    - dnf install -y environment-modules 
    - dnf install -y python3-mpi4py-openmpi 
    - dnf install -y pandoc
    - dnf install -y sqlite-devel
    - wget https://www.python.org/ftp/python/3.10.14/Python-3.10.14.tgz 
    - tar xzf Python-3.10.14.tgz 
    - cd Python-3.10.14 
    - ./configure --with-system-ffi --with-computed-gotos --enable-loadable-sqlite-extensions 
    - make -j ${nproc} 
    - make altinstall 
    - cd ..
    - pwd
    - ls  
    - export OMPI_ALLOW_RUN_AS_ROOT=1
    - export OMPI_ALLOW_RUN_AS_ROOT_CONFIRM=1
    - mv /usr/local/lib/libpython3.10.a libpython3.10.a.bak
  script:
    - python3.10 -V
    - python3.10 -m venv env
    - source env/bin/activate
    - . /etc/profile.d/modules.sh
    - module load mpi/openmpi-$(arch)
    - module list 
    - echo $TEST_SCHEDULED
    - pip install --no-cache-dir $(echo $STRUPHY_SOURCE_DOCKER)[test]
    - struphy compile --time-execution 
    - struphy test models --fast
    - struphy compile --time-execution --language=fortran -y 
    - struphy test models --fast

struphy_fedora_latest:
  stage: test
  image: gitlab-registry.mpcdf.mpg.de/struphy/struphy/struphy_fedora_latest
  rules:
    # Temporary: Since this docker image uses python3.12, pip install -U struphy will always install 1.9.9,
    # because it is the highest version that does not have the requirement Python<3.12. Therefore, we will
    # skip this job when the scheduled pipeline is launched with the variable $STRUPHY_SOURCE_DOCKER == "-U struphy"
    - if: $STRUPHY_SOURCE_DOCKER == "-U struphy"
      when: never
    - if: $CI_PIPELINE_SOURCE == "schedule"
    - if: $TEST_SCHEDULED == "true"
  needs: ['default_build']
  before_script:
    - date
    - uname -a
    - ls -a
    - ls dist/
    - ls /
  script:
    - python3 -V
    - python3 -m venv env
    - source env/bin/activate
    - . /etc/profile.d/modules.sh
    - module load mpi/openmpi-$(arch)
    - module list 
    - echo $CI_COMMIT_MESSAGE
    - echo $TEST_SCHEDULED
    - pip install --no-cache-dir $(echo $STRUPHY_SOURCE_DOCKER)[test]
    - struphy compile --time-execution 
    - struphy test models --fast
    - struphy compile --time-execution --language=fortran -y 
    - struphy test models --fast

OpenSuse:
  stage: test
  image: opensuse/tumbleweed:latest
  rules:
    - if: $CI_PIPELINE_SOURCE == "schedule"
    - if: $TEST_SCHEDULED == "true"
  needs: ['default_build']
  before_script:
    - date
    - ls -a
    - zypper refresh
    - zypper install -y python311 python311-devel
    - zypper install -y python311-pip python3-virtualenv
    - zypper install -y gcc-fortran gcc 
    - zypper install -y lapack-devel openmpi-devel 
    - zypper install -y blas-devel openmpi 
    - zypper install -y libgomp1 
    - zypper install -y git 
    - zypper install -y pandoc 
    - zypper install -y sqlite3 
    - zypper install -y vim 
    - zypper install -y make
    - python3 -m venv /opensuse_latest/venv
    - export PATH="/opensuse_latest/venv/bin:$PATH"
    - export OMPI_ALLOW_RUN_AS_ROOT=1
    - export OMPI_ALLOW_RUN_AS_ROOT_CONFIRM=1
    - export OMPI_MCA_pml=ob1
    - export OMPI_MCA_btl=tcp,self
    - export PATH="/usr/lib64/mpi/gcc/openmpi4/bin/:$PATH"
    - export LD_LIBRARY_PATH="/usr/lib64/mpi/gcc/openmpi4/lib64:$LD_LIBRARY_PATH"
  script:
    - python3 -V
    - python3 -m pip list
    - python3 -m venv env
    - source env/bin/activate
    - echo $CI_COMMIT_MESSAGE
    - echo $TEST_SCHEDULED
    - pip install --no-cache-dir $(echo $STRUPHY_SOURCE_DOCKER)[test]
    - struphy compile --time-execution 
    - struphy test models --fast
    - struphy compile --time-execution --language=fortran -y 
    - struphy test models --fast

struphy_opensuse_latest:
  stage: test
  image: gitlab-registry.mpcdf.mpg.de/struphy/struphy/struphy_opensuse_latest
  rules:
    # Temporary: Since this docker image uses python3.12, pip install -U struphy will always install 1.9.9,
    # because it is the highest version that does not have the requirement Python<3.12. Therefore, we will
    # skip this job when the scheduled pipeline is launched with the variable $STRUPHY_SOURCE_DOCKER == "-U struphy"
    - if: $STRUPHY_SOURCE_DOCKER == "-U struphy"
      when: never
    - if: $CI_PIPELINE_SOURCE == "schedule"
    - if: $TEST_SCHEDULED == "true"
  needs: ['default_build']
  before_script:
    - date
    - uname -a
    - ls -a
    - ls dist/
    - ls /
  script:
    - set -x
    - python3 -V
    - python3 -m pip list
    - python3 -m venv env
    - source env/bin/activate
    - echo $CI_COMMIT_MESSAGE
    - echo $TEST_SCHEDULED
    - pip install --upgrade pip
    - pip install --no-cache-dir $(echo $STRUPHY_SOURCE_DOCKER)[test]
    - struphy compile --time-execution
    - struphy test models --fast
    - struphy compile --time-execution --language=fortran -y
    - struphy test models --fast

AlmaLinux:
  stage: test
  image: almalinux:latest
  rules:
    - if: $CI_PIPELINE_SOURCE == "schedule"
    - if: $TEST_SCHEDULED == "true"
  needs: ['default_build']
  before_script:
    - date
    - ls -a
    - yum install -y wget yum-utils make openssl-devel bzip2-devel libffi-devel zlib-devel
    - yum update -y 
    - yum clean all 
    - yum install -y gcc 
    - yum install -y gfortran  
    - yum install -y openmpi openmpi-devel  
    - yum install -y libgomp 
    - yum install -y git 
    - yum install -y environment-modules 
    - yum install -y sqlite-devel
    - wget https://www.python.org/ftp/python/3.10.14/Python-3.10.14.tgz 
    - tar xzf Python-3.10.14.tgz 
    - cd Python-3.10.14 
    - ./configure --with-system-ffi --with-computed-gotos --enable-loadable-sqlite-extensions 
    - make -j ${nproc} 
    - make altinstall 
    - cd ..
    - pwd
    - ls
    - export OMPI_ALLOW_RUN_AS_ROOT=1
    - export OMPI_ALLOW_RUN_AS_ROOT_CONFIRM=1
    - export OMPI_MCA_pml=ob1
    - export OMPI_MCA_btl=tcp,self
    - export PATH="/usr/lib64/openmpi/bin:$PATH"
    - mv /usr/local/lib/libpython3.10.a libpython3.10.a.bak
  script:
    - python3.10 -m pip list
    - python3.10 -m venv env
    - source env/bin/activate
    - echo $CI_COMMIT_MESSAGE
    - echo $TEST_SCHEDULED
    - pip install --no-cache-dir $(echo $STRUPHY_SOURCE_DOCKER)[test]
    - pip list
    - struphy compile --time-execution 
    - struphy test models --fast
    - struphy compile --time-execution --language=fortran -y 
    - struphy test models --fast

struphy_almalinux_latest:
  stage: test
  image: gitlab-registry.mpcdf.mpg.de/struphy/struphy/struphy_almalinux_latest
  rules:
    # Temporary: Since this docker image uses python3.12, pip install -U struphy will always install 1.9.9,
    # because it is the highest version that does not have the requirement Python<3.12. Therefore, we will
    # skip this job when the scheduled pipeline is launched with the variable $STRUPHY_SOURCE_DOCKER == "-U struphy"
    - if: $STRUPHY_SOURCE_DOCKER == "-U struphy"
      when: never
    - if: $CI_PIPELINE_SOURCE == "schedule"
    - if: $TEST_SCHEDULED == "true"
  needs: ['default_build']
  before_script:
    - date
    - uname -a
    - ls -a
    - ls dist/
    - ls /
  script:
    - python3 -m ensurepip --upgrade
    - python3 -m pip install --upgrade pip
    - python3 -m pip list
    - python3 -V
    - python3 -m venv env
    - source env/bin/activate
    - echo $CI_COMMIT_MESSAGE
    - echo $TEST_SCHEDULED
    - pip install --no-cache-dir $(echo $STRUPHY_SOURCE_DOCKER)[test]
    - pip list
    - struphy compile --time-execution 
    - struphy test models --fast
    - struphy compile --time-execution --language=fortran -y 
    - struphy test models --fast

###################
### PAGES STAGE ###
###################

pages:
  image: gitlab-registry.mpcdf.mpg.de/struphy/struphy/struphy_ubuntu_latest
  stage: pages
  rules:
    - if: $TEST_SCHEDULED == "true"
      when: never
    - if: $CI_PIPELINE_SOURCE == "schedule"
      when: never
    - if: $CI_COMMIT_TAG
      when: never
    - if: $CI_COMMIT_REF_NAME == "master"
    - if: $MAKE_PAGES == "true"
  needs: ['default_build']
  before_script:
    - date
    - uname -a
    - ls -a
    - ls dist/
    - ls /
    - apt show pandoc
    - apt show graphviz
    - python3 -V
    - python3 -V
    - python3 -m venv env
    - source env/bin/activate
    - pip install --upgrade pip
    - echo $CI_COMMIT_MESSAGE
    - pip install $(echo dist/struphy*.whl)[doc]
  script:
    - struphy -h
    - struphy compile --status
    - struphy compile --time-execution -y
    #- struphy test timings
    #- ls $(python3 -c "import struphy as _; print(_.__path__[0])")/_pymon --all
    - cd doc ; make html
    - mv _build/html/ ../public/
  artifacts:
    name: 'pages'
    paths:
      - public/

#####################
### RELEASE STAGE ###
#####################

deploy:
  stage: release
  rules:
    - if: $TEST_SCHEDULED == "true"
      when: never
    - if: $CI_PIPELINE_SOURCE == "schedule"
      when: never
    - if: $CI_COMMIT_TAG
      when: never
    - if: $CI_COMMIT_REF_NAME == "master" 
  script:
    - twine upload dist/*

vars:
  stage: release
  rules:
    - if: $TEST_SCHEDULED == "true"
      when: never
    - if: $CI_PIPELINE_SOURCE == "schedule"
      when: never
    - if: $CI_COMMIT_TAG
      when: never
    - if: $CI_COMMIT_REF_NAME == "master"
  before_script:
    - date
    - ls -a
    - uname -s # get system info
  script:
    - echo "VERSION=$(var=$(<pyproject.toml); set -- $var; echo ${14} | sed 's/^.//' | sed 's/.$//')" >> vars.env
  artifacts:
    name: 'vars'
    reports:
      dotenv: vars.env
    expire_in: 1 day

release_job:
  stage: release
  image: registry.gitlab.com/gitlab-org/release-cli:latest
  rules:
    - if: $TEST_SCHEDULED == "true"
      when: never
    - if: $CI_PIPELINE_SOURCE == "schedule"
      when: never
    - if: $CI_COMMIT_TAG
      when: never
    - if: $CI_COMMIT_REF_NAME == "master"
  needs: ['vars']
  before_script:
    - date
    - ls -a
    - uname -s # get system info
  script:
    - cat /etc/*-release
    - echo $VERSION
  release:                                         # See https://docs.gitlab.com/ee/ci/yaml/#release for available properties
    tag_name: 'v$VERSION'                          # The version is incremented per pipeline.
    name: 'v$VERSION'
    ref: '$CI_COMMIT_SHA'                          # The tag is created from the pipeline SHA.
    description: 'CHANGELOG.md'
    assets:
      links:
        - name: 'Documentation'
          url: 'https://struphy.pages.mpcdf.de/struphy/index.html'
        - name: 'PyPI'
          url: 'https://pypi.org/project/struphy/'


