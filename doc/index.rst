Struphy: structure-preserving hybrid codes
==========================================

**A Python package for plasma physics PDEs.**

.. image:: pics/progress_ipp.png
    :align: right
    :scale: 40

Struphy is open-source (https://gitlab.mpcdf.mpg.de/struphy/struphy) and can be :ref:`installed on any architecture <install>`.

The package is designed for the parallel solution of partial differential equations based
on :ref:`geomFE` and :ref:`particle_discrete`.

Struphy features a :ref:`growing list of PDE models <models>` 
which can be solved on a variety of :ref:`mapped domains <avail_mappings>`.

| Struphy can be improved by you (see :ref:`developers`).
| To become a developer, you need a Max-Planck Gitlab (https://gitlab.mpcdf.mpg.de/) account. 
| In case you are not affiliated with the Max Planck Society, please :ref:`contact` a Max Planck employee for an invitation.
| For further information on forking and merge- (or pull-) requests please go to :ref:`git_workflow`.

.. toctree::
   :maxdepth: 1
   :caption: Contents:

   sections/abstract
   sections/install
   sections/quickstart
   sections/userguide
   sections/tutorials
   sections/numerics
   sections/models
   sections/domains
   sections/equilibria
   sections/propagators
   sections/pic_classes
   sections/feec_classes
   sections/utilities
   sections/developers


Reference paper
---------------

\S. Possanner, F. Holderied, Y. Li, B.-K. Na, D. Bell, S. Hadjout and Y. Güçlü, `High-Order Structure-Preserving Algorithms for Plasma Hybrid Models <https://link.springer.com/chapter/10.1007/978-3-031-38299-4_28>`_, International Conference on Geometric Science of Information 2023, 263-271, Springer Nature Switzerland.


.. _contact:

Contact
-------

Struphy is constantly maintained. Please contact 

* stefan.possanner@ipp.mpg.de
* eric.sonnendruecker@ipp.mpg.de
* xin.wang@ipp.mpg.de

for questions.




   


