{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 6 - Poisson equation\n",
    "\n",
    "Topics covered in this tutorial:\n",
    "\n",
    "- instantiate [Derham](https://struphy.pages.mpcdf.de/struphy/sections/subsections/feec_derham.html#module-struphy.feec.psydac_derham) objects for different dimensions\n",
    "- creating callable FE fields\n",
    "- use of [Projections](https://struphy.pages.mpcdf.de/struphy/sections/subsections/feec_projectors.html#module-struphy.feec.projectors) into Derham\n",
    "- instantiate [WeigthedMassOperators](https://struphy.pages.mpcdf.de/struphy/sections/subsections/feec_weightedmass.html#module-struphy.feec.mass) object\n",
    "- use of [ImplicitDiffusion](https://struphy.pages.mpcdf.de/struphy/sections/subsections/propagators_fields.html#struphy.propagators.propagators_fields.ImplicitDiffusion) propagator\n",
    "\n",
    "In what follows we present some examples of the following problem:\n",
    "let $\\Omega \\subset \\mathbb R^d$ be open. We want to find $\\phi \\in H^1(\\Omega)$ such that\n",
    "\n",
    "$$\n",
    "- \\nabla \\cdot  \\,\\nabla \\phi(\\mathbf x) + \\epsilon \\,\\phi(\\mathbf x)  = \\rho(\\mathbf x)\\qquad \\mathbf x \\in \\Omega\\,,\n",
    "$$\n",
    "\n",
    "for suitable boundary conditions, where $\\epsilon \\in \\mathbb R$ is a constant and $\\rho: \\Omega \\to \\mathbb R^+$ is a positive function.\n",
    "\n",
    "For this we can use the Propagator [ImplicitDiffusion](https://struphy.pages.mpcdf.de/struphy/sections/subsections/propagators_fields.html#struphy.propagators.propagators_fields.ImplicitDiffusion)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from struphy.propagators.propagators_fields import ImplicitDiffusion\n",
    "\n",
    "# default parameters of the Propagator\n",
    "opts = ImplicitDiffusion.options(default=True)\n",
    "opts"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Manufactured solution in 1D"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# set up Derham complex\n",
    "from struphy.feec.psydac_derham import Derham\n",
    "\n",
    "Nel = [32, 1, 1]\n",
    "p = [1, 1, 1]\n",
    "spl_kind = [True, True, True]\n",
    "derham = Derham(Nel, p, spl_kind)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# set up domain Omega\n",
    "from struphy.geometry.domains import Cuboid\n",
    "import numpy as np\n",
    "\n",
    "l1 = -2*np.pi\n",
    "r1 = 2*np.pi\n",
    "domain = Cuboid(l1=l1, r1=r1)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# set up mass matrices\n",
    "from struphy.feec.mass import WeightedMassOperators\n",
    "\n",
    "mass_ops = WeightedMassOperators(derham, domain)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# pass simulation parameters to Propagator\n",
    "ImplicitDiffusion.derham = derham\n",
    "ImplicitDiffusion.domain = domain\n",
    "ImplicitDiffusion.mass_ops = mass_ops"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# create solution field in Vh_0 subset H1 \n",
    "phi = derham.create_field('my solution', 'H1')\n",
    "phi"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "phi.vector"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# manufactured solution, defined on Omega\n",
    "k = 2\n",
    "f_xyz = lambda x, y, z: np.sin(k*x)\n",
    "rhs_xyz = lambda x, y, z: k**2 * np.sin(k*x)\n",
    "\n",
    "# pullback to the logical unit cube \n",
    "rhs = lambda e1, e2, e3: domain.pull(rhs_xyz, e1, e2, e3)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# compute rhs vector in Vh_0 subset H1 \n",
    "from struphy.feec.projectors import L2Projector\n",
    "\n",
    "l2proj = L2Projector('H1', mass_ops)\n",
    "\n",
    "rho = l2proj.get_dofs(rhs)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# equation parameters\n",
    "eps = 1e-12\n",
    "\n",
    "# instantiate Propagator for the above quation, pass data structure (vector) of FemField\n",
    "poisson = ImplicitDiffusion(phi.vector, sigma_1=eps, rho=rho)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# solve (call with arbitrary dt)\n",
    "poisson(1.)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# evalaute at logical coordinates\n",
    "e1 = np.linspace(0, 1, 100)\n",
    "e2 = .5\n",
    "e3 = .5\n",
    "\n",
    "funval = phi(e1, e2, e3)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# push to Omega\n",
    "fh_xyz = domain.push(funval, e1, e2, e3, squeeze_out=True)\n",
    "\n",
    "x, y, z = domain(e1, e2, e3, squeeze_out=True)\n",
    "x.shape"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# plot solution \n",
    "from matplotlib import pyplot as plt\n",
    "\n",
    "plt.plot(x, f_xyz(x, 0., 0.), label='exact')\n",
    "plt.plot(x, fh_xyz, '--r', label='numeric')\n",
    "plt.xlabel('x')\n",
    "plt.legend();"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Manufactured solution in a Torus\n",
    "\n",
    "Under construction ..."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.12"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
