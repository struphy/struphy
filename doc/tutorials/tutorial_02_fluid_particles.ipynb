{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 2 - Fluid particles\n",
    "\n",
    "Topics covered in this tutorial:\n",
    "\n",
    "- basic functionalities of [ParticlesSPH](https://struphy.pages.mpcdf.de/struphy/sections/subsections/pic_base.html#base-modules) class\n",
    "- initializing velocities as $\\mathbf v(0) = \\mathbf u(\\mathbf x(0))$ via a [GenericFluidEquilibrium](https://struphy.pages.mpcdf.de/struphy/sections/subsections/mhd_equils_sub.html#generic-fluid-equilibria)\n",
    "- velocity push with [PushVinEfield](https://struphy.pages.mpcdf.de/struphy/sections/subsections/propagators_markers.html#struphy.propagators.propagators_markers.PushVinEfield)\n",
    "- velocity push with [PushVinSPHpressure](https://struphy.pages.mpcdf.de/struphy/sections/subsections/propagators_markers.html#struphy.propagators.propagators_markers.PushVinSPHpressure)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Fluid flow in external force field\n",
    "\n",
    "Let $\\Omega \\subset \\mathbb R^3$ be a box (cuboid). We search for trajectories $(\\mathbf x_p, \\mathbf v_p): [0,T] \\to \\Omega \\times \\mathbb R^3$, $p = 0, \\ldots, N-1$ that satisfy\n",
    "\n",
    "$$\n",
    "\\begin{align}\n",
    " \\dot{\\mathbf x}_p &= \\mathbf v_p\\,,\\qquad && \\mathbf x_p(0) = \\mathbf x_{p0}\\,,\n",
    " \\\\[2mm]\n",
    " \\dot{\\mathbf v}_p &= -\\nabla p(\\mathbf x_p) \\qquad && \\mathbf v_p(0) = \\mathbf u(\\mathbf x_p(0))\\,,\n",
    " \\end{align}\n",
    "$$\n",
    "\n",
    "where $p \\in H^1(\\Omega)$ is some given function.\n",
    "In Struphy, the position coordinates are updated in logical space $[0, 1]^3 = F^{-1}(\\Omega)$, for instance with the Propagator [PushEta](https://struphy.pages.mpcdf.de/struphy/sections/subsections/propagators_markers.html#struphy.propagators.propagators_markers.PushEta) which we shall use in what follows."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from struphy.geometry.domains import Cuboid\n",
    "\n",
    "l1 = -.5\n",
    "r1 = .5\n",
    "l2 = -.5\n",
    "r2 = .5\n",
    "l3 = 0.\n",
    "r3 = 1.\n",
    "domain = Cuboid(l1=l1, r1=r1, l2=l2, r2=r2, l3=l3, r3=r3)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from struphy.fields_background.generic import GenericCartesianFluidEquilibrium\n",
    "import numpy as np\n",
    "\n",
    "def u_fun(x, y, z):\n",
    "    ux = -np.cos(np.pi*x)*np.sin(np.pi*y)\n",
    "    uy = np.sin(np.pi*x)*np.cos(np.pi*y)\n",
    "    uz = 0 * x \n",
    "    return ux, uy, uz\n",
    "\n",
    "p_fun = lambda x, y, z: 0.5*(np.sin(np.pi*x)**2 + np.sin(np.pi*y)**2)\n",
    "n_fun = lambda x, y, z: 1. + 0*x\n",
    "\n",
    "bel_flow = GenericCartesianFluidEquilibrium(u_xyz=u_fun, p_xyz=p_fun, n_xyz=n_fun)\n",
    "bel_flow.domain = domain\n",
    "p_xyz = bel_flow.p_xyz\n",
    "p0 = bel_flow.p0"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from struphy.pic.particles import ParticlesSPH\n",
    "\n",
    "Np = 1000\n",
    "bc = ['reflect', 'reflect', 'periodic']\n",
    "\n",
    "# instantiate Particle object\n",
    "particles = ParticlesSPH(\n",
    "        Np=Np,\n",
    "        bc=bc,\n",
    "        domain=domain,\n",
    "        bckgr_params=bel_flow,\n",
    "    )"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "particles.draw_markers(sort=False)\n",
    "particles.apply_kinetic_bc()\n",
    "particles.initialize_weights()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "particles.positions\n",
    "# positions on the physical domain Omega\n",
    "pushed_pos = domain(particles.positions).T\n",
    "pushed_pos"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from struphy.propagators.propagators_markers import PushEta\n",
    "\n",
    "# default parameters of Propagator\n",
    "opts_eta = PushEta.options(default=False)\n",
    "print(opts_eta)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# pass simulation parameters to Propagator class\n",
    "PushEta.domain = domain"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# instantiate Propagator object\n",
    "prop_eta = PushEta(particles, algo = \"forward_euler\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from struphy.feec.psydac_derham import Derham\n",
    "\n",
    "Nel = [64, 64, 1]  # Number of grid cells\n",
    "p = [3, 3, 1]  # spline degrees\n",
    "spl_kind = [False, False, True]   # spline types (clamped vs. periodic)\n",
    "\n",
    "derham = Derham(Nel, p, spl_kind)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "p_coeffs = derham.P[\"0\"](p0)\n",
    "p_coeffs"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from struphy.propagators.propagators_markers import PushVinEfield\n",
    "\n",
    "# instantiate Propagator object\n",
    "PushVinEfield.domain = domain\n",
    "PushVinEfield.derham = derham"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "p_h = derham.create_field('pressure', 'H1')\n",
    "p_h.vector = p_coeffs"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import matplotlib.pyplot as plt\n",
    "import numpy as np\n",
    "\n",
    "plt.figure(figsize=(12, 12))\n",
    "x = np.linspace(-.5, .5, 100)\n",
    "y = np.linspace(-.5, .5, 90)\n",
    "xx, yy = np.meshgrid(x, y)\n",
    "eta1 = np.linspace(0, 1, 100)\n",
    "eta2 = np.linspace(0, 1, 90)\n",
    "\n",
    "plt.subplot(2, 2, 1)\n",
    "plt.pcolor(xx, yy, p_xyz(xx, yy, 0))\n",
    "plt.axis('square')\n",
    "plt.title('p_xyz')\n",
    "plt.colorbar()\n",
    "\n",
    "plt.subplot(2, 2, 2)\n",
    "p_vals = p0(eta1, eta2, 0, squeeze_out=True).T\n",
    "plt.pcolor(eta1, eta2, p_vals)\n",
    "plt.axis('square')\n",
    "plt.title('p logical')\n",
    "plt.colorbar()\n",
    "\n",
    "plt.subplot(2, 2, 3)\n",
    "p_h_vals = p_h(eta1, eta2, 0, squeeze_out=True).T\n",
    "plt.pcolor(eta1, eta2, p_h_vals)\n",
    "plt.axis('square')\n",
    "plt.title('p_h (logical)')\n",
    "plt.colorbar()\n",
    "\n",
    "plt.subplot(2, 2, 4)\n",
    "plt.pcolor(eta1, eta2, np.abs(p_vals - p_h_vals))\n",
    "plt.axis('square')\n",
    "plt.title('difference')\n",
    "plt.colorbar()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "grad_p = derham.grad.dot(p_coeffs)\n",
    "grad_p.update_ghost_regions() # very important, we will move it inside grad\n",
    "grad_p *= -1.\n",
    "prop_v = PushVinEfield(particles, e_field=grad_p)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import math\n",
    "import tqdm\n",
    "\n",
    "# time stepping\n",
    "dt = 0.02\n",
    "Nt = 200\n",
    "\n",
    "pos = np.zeros((Nt + 1, Np, 3), dtype=float)\n",
    "velo = np.zeros((Nt + 1, Np, 3), dtype=float)\n",
    "energy = np.zeros((Nt + 1, Np), dtype=float)\n",
    "\n",
    "particles.draw_markers(sort=False)\n",
    "particles.apply_kinetic_bc()\n",
    "particles.initialize_weights()\n",
    "\n",
    "pos[0] = domain(particles.positions).T\n",
    "velo[0] = particles.velocities\n",
    "energy[0] = .5*(velo[0, : , 0]**2 + velo[0, : , 1]**2) + p_h(particles.positions)\n",
    "\n",
    "time = 0.\n",
    "time_vec = np.zeros(Nt + 1, dtype=float)\n",
    "n = 0\n",
    "while n < Nt:\n",
    "    time += dt\n",
    "    n += 1\n",
    "    time_vec[n] = time\n",
    "    \n",
    "    # advance in time\n",
    "    prop_eta(dt/2)\n",
    "    prop_v(dt)\n",
    "    prop_eta(dt/2)\n",
    "    \n",
    "    # positions on the physical domain Omega\n",
    "    pos[n] = domain(particles.positions).T\n",
    "    velo[n] = particles.velocities\n",
    "    \n",
    "    energy[n] = .5*(velo[n, : , 0]**2 + velo[n, : , 1]**2) + p_h(particles.positions)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# energy plots\n",
    "fig = plt.figure(figsize = (13, 6))\n",
    "\n",
    "plt.subplot(2, 2, 1)\n",
    "plt.plot(time_vec, energy[:, 0])\n",
    "plt.title('particle 1')\n",
    "plt.xlabel('time')\n",
    "plt.ylabel('energy')\n",
    "\n",
    "plt.subplot(2, 2, 2)\n",
    "plt.plot(time_vec, energy[:, 1])\n",
    "plt.title('particle 2')\n",
    "plt.xlabel('time')\n",
    "plt.ylabel('energy')\n",
    "\n",
    "plt.subplot(2, 2, 3)\n",
    "plt.plot(time_vec, energy[:, 2])\n",
    "plt.title('particle 3')\n",
    "plt.xlabel('time')\n",
    "plt.ylabel('energy')\n",
    "\n",
    "plt.subplot(2, 2, 4)\n",
    "plt.plot(time_vec, energy[:, 3])\n",
    "plt.title('particle 4')\n",
    "plt.xlabel('time')\n",
    "plt.ylabel('energy')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig = plt.figure()\n",
    "ax = fig.add_subplot(projection=\"3d\")\n",
    "ax.scatter(pos[-1,:,0],pos[-1,:,1],pos[-1,:,2])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.figure(figsize=(12, 28))\n",
    "\n",
    "coloring = np.select([pos[0,:,0]<=-0.2, np.abs(pos[0,:,0]) < +0.2, pos[0,:,0] >= 0.2],\n",
    "                        [-1.0, 0.0, +1.0])\n",
    "\n",
    "interval = Nt/20\n",
    "plot_ct = 0\n",
    "for i in range(Nt):\n",
    "    if i % interval == 0:\n",
    "        print(f'{i = }')\n",
    "        plot_ct += 1\n",
    "        plt.subplot(5, 2, plot_ct)\n",
    "        ax = plt.gca() \n",
    "        plt.scatter(pos[i, :, 0], pos[i, :, 1], c=coloring)\n",
    "        plt.axis('square')\n",
    "        plt.title('n0_scatter')\n",
    "        plt.xlim(l1, r1)\n",
    "        plt.ylim(l2, r2)\n",
    "        plt.colorbar()\n",
    "        plt.title(f'Gas at t={i*dt}')\n",
    "    if plot_ct == 10:\n",
    "        break"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "make_movie = False\n",
    "if make_movie:\n",
    "    import matplotlib.animation as animation\n",
    "    n_frame = Nt\n",
    "    fig, ax = plt.subplots()\n",
    "\n",
    "    coloring = np.select([pos[0,:,0]<=-0.2, np.abs(pos[0,:,0]) < +0.2, pos[0,:,0] >= 0.2],\n",
    "                        [-1.0, 0.0, +1.0])\n",
    "    scat = ax.scatter(pos[0,:,0], pos[0,:,1], c=coloring)\n",
    "    ax.set_xlim([-0.5,0.5])\n",
    "    ax.set_ylim([-0.5,0.5])\n",
    "    ax.set_aspect('equal')\n",
    "\n",
    "    f = lambda x, y: np.cos(np.pi*x)*np.cos(np.pi*y)\n",
    "    ax.contour(xx, yy, f(xx, yy))\n",
    "    ax.set_title(f'time = {time_vec[0]:4.2f}')\n",
    "\n",
    "    def update_frame(frame):\n",
    "        scat.set_offsets(pos[frame,:,:2])\n",
    "        ax.set_title(f'time = {time_vec[frame]:4.2f}')\n",
    "        return scat\n",
    "\n",
    "    ani = animation.FuncAnimation(fig=fig, func=update_frame, frames = n_frame)\n",
    "    ani.save(\"tutorial_02_movie.gif\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Gas expansion\n",
    "\n",
    "We use SPH to solve Euler's equations\n",
    "\n",
    "$$\n",
    "\\begin{align}\n",
    " \\partial_t \\rho + \\nabla \\cdot (\\rho \\mathbf u) &= 0\\,,\n",
    " \\\\[2mm]\n",
    " \\rho(\\partial_t \\mathbf u + \\mathbf u \\cdot \\nabla \\mathbf u) &= - \\nabla \\left(\\rho^2 \\frac{\\partial \\mathcal U(\\rho, S)}{\\partial \\rho} \\right)\\,,\n",
    " \\\\[2mm]\n",
    " \\partial_t S + \\mathbf u \\cdot \\nabla S &= 0\\,,\n",
    " \\end{align}\n",
    "$$\n",
    "\n",
    "where $S$ denotes the entropy per unit mass and the internal energy per unit mass is \n",
    "\n",
    "$$\n",
    "\\mathcal U(\\rho, S) = \\kappa(S) \\log \\rho\\,.\n",
    "$$\n",
    "\n",
    "The SPH discretization leads to ODEs for $N$ particles indexed by $p$,\n",
    "\n",
    "$$\n",
    "\\begin{align}\n",
    " \\dot{\\mathbf x}_p &= \\mathbf v_p\\,,\\qquad && \\mathbf x_p(0) = \\mathbf x_{p0}\\,,\n",
    " \\\\[2mm]\n",
    " \\dot{\\mathbf v}_p &= -\\kappa_{p}(0) \\sum_{q=1}^N w_p w_q \\left(\\frac{1}{\\rho^{N,h}(\\mathbf x_p)} + \\frac{1}{\\rho^{N,h}(\\mathbf x_q)} \\right) \\nabla W_h(\\mathbf x_p - \\mathbf x_q) \\qquad && \\mathbf v_p(0) = \\mathbf u(\\mathbf x_p(0))\\,,\n",
    " \\end{align}\n",
    "$$\n",
    "\n",
    "where the smoothed density reads\n",
    "\n",
    "$$\n",
    " \\rho^{N,h}(\\mathbf x) = \\sum_{p=1}^N w_p W_h(\\mathbf x - \\mathbf x_p)\\,,\n",
    "$$\n",
    "\n",
    "with weights $w_p = const.$ and where $W_h(\\mathbf x)$ is a suitable smoothing kernel.\n",
    "The velocity update is performed with the Propagator [PushVinSPHpressure](https://struphy.pages.mpcdf.de/struphy/sections/subsections/propagators_markers.html#struphy.propagators.propagators_markers.PushVinSPHpressure)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from struphy.geometry.domains import Cuboid\n",
    "\n",
    "l1 = -3\n",
    "r1 = 3\n",
    "l2 = -3\n",
    "r2 = 3\n",
    "l3 = 0.\n",
    "r3 = 1.\n",
    "domain = Cuboid(l1=l1, r1=r1, l2=l2, r2=r2, l3=l3, r3=r3)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from struphy.fields_background.generic import GenericCartesianFluidEquilibrium\n",
    "import numpy as np\n",
    "T_h = 0.2\n",
    "gamma = 5/3\n",
    "n_fun = lambda x, y, z: np.exp(-(x**2 + y**2)/T_h)\n",
    "\n",
    "bckgr = GenericCartesianFluidEquilibrium(n_xyz=n_fun)\n",
    "bckgr.domain = domain"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#particle initialization \n",
    "from struphy.pic.particles import ParticlesSPH\n",
    "\n",
    "# marker parameters\n",
    "ppb = 400\n",
    "nx = 16\n",
    "ny = 16\n",
    "nz = 1\n",
    "boxes_per_dim = (nx, ny, nz)\n",
    "bc = ['periodic']*3\n",
    "\n",
    "# instantiate Particle object\n",
    "particles = ParticlesSPH(\n",
    "        #Np=10000,\n",
    "        ppb=ppb,\n",
    "        boxes_per_dim=boxes_per_dim,\n",
    "        bc=bc,\n",
    "        domain=domain,\n",
    "        bckgr_params=bckgr,\n",
    "        verbose_boxes=False,\n",
    "    )"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "threshold = 1e-1\n",
    "particles.draw_markers(sort=False)\n",
    "particles.apply_kinetic_bc()\n",
    "particles.initialize_weights(reject_weights=True, threshold=threshold)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "particles.markers.shape"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "particles.sorting_boxes.boxes.shape"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "components = [True, True, False, False, False, False]\n",
    "nx_b = 64\n",
    "ny_b = 64\n",
    "be_x = np.linspace(0, 1, nx_b + 1)\n",
    "be_y = np.linspace(0, 1, ny_b + 1)\n",
    "bin_edges = [be_x, be_y]\n",
    "f_bin, df_bin = particles.binning(components, bin_edges, divide_by_jac=False)\n",
    "f_bin.shape"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "x = np.linspace(l1, r1, 100)\n",
    "y = np.linspace(l2, r2, 90)\n",
    "xx, yy = np.meshgrid(x, y)\n",
    "eta1 = np.linspace(0, 1, 100)\n",
    "eta2 = np.linspace(0, 1, 90)\n",
    "eta3 = np.linspace(0,1,1)\n",
    "ee1, ee2, ee3 = np.meshgrid(eta1, eta2, eta3)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "kernel_type = \"gaussian_2d\" \n",
    "h1 = 1/nx\n",
    "h2 = 1/ny\n",
    "h3 = 1/nz\n",
    "\n",
    "n_sph = particles.eval_density(ee1, ee2, ee3, h1=h1, h2=h2, h3=h3, kernel_type=kernel_type, fast=True,)\n",
    "n_sph.shape"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "logpos = particles.positions\n",
    "weights = particles.weights\n",
    "print(f'{logpos.shape = }')\n",
    "print(f'{weights.shape = }')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import matplotlib.pyplot as plt \n",
    "plt.figure(figsize=(12, 12))\n",
    "\n",
    "n_xyz = bckgr.n_xyz\n",
    "n0 = bckgr.n0\n",
    "\n",
    "plt.subplot(3, 2, 1)\n",
    "plt.pcolor(xx, yy, n_fun(xx, yy, 0))\n",
    "plt.axis('square')\n",
    "plt.title('n_xyz')\n",
    "plt.colorbar()\n",
    "\n",
    "plt.subplot(3, 2, 2)\n",
    "plt.pcolor(eta1, eta2, n0(eta1,eta2,0, squeeze_out=True).T)\n",
    "plt.axis('square')\n",
    "plt.title('n_0')\n",
    "plt.colorbar()\n",
    "\n",
    "make_scatter = True\n",
    "if make_scatter:\n",
    "    plt.subplot(3, 2, 3)\n",
    "    ax = plt.gca()\n",
    "    ax.set_xticks(np.linspace(0, 1, nx + 1))\n",
    "    ax.set_yticks(np.linspace(0, 1., ny + 1))\n",
    "    plt.tick_params(labelbottom = False) \n",
    "    coloring = weights\n",
    "    plt.scatter(logpos[:, 0], logpos[:, 1], c=coloring, s=.25)\n",
    "    plt.grid(c='k')\n",
    "    plt.axis('square')\n",
    "    plt.title('n0_scatter')\n",
    "    plt.xlim(0, 1)\n",
    "    plt.ylim(0, 1)\n",
    "    plt.colorbar()\n",
    "\n",
    "plt.subplot(3, 2, 4)\n",
    "ax = plt.gca()\n",
    "ax.set_xticks(np.linspace(0, 1, nx + 1))\n",
    "ax.set_yticks(np.linspace(0, 1., ny + 1))\n",
    "plt.tick_params(labelbottom = False) \n",
    "plt.pcolor(ee1[:,:,0], ee2[:,:,0], n_sph[:,:,0])\n",
    "plt.grid()\n",
    "plt.axis('square')\n",
    "plt.title(f'n_sph')\n",
    "plt.colorbar()\n",
    "\n",
    "plt.subplot(3, 2, 6)\n",
    "ax = plt.gca()\n",
    "# ax.set_xticks(np.linspace(0, 1, nx + 1))\n",
    "# ax.set_yticks(np.linspace(0, 1., ny + 1))\n",
    "# plt.tick_params(labelbottom = False) \n",
    "bc_x = (be_x[:-1] + be_x[1:]) / 2. # centers of binning cells\n",
    "bc_y = (be_y[:-1] + be_y[1:]) / 2.\n",
    "plt.pcolor(bc_x, bc_y, f_bin)\n",
    "#plt.grid()\n",
    "plt.axis('square')\n",
    "plt.title(f'n_binned')\n",
    "plt.colorbar()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from struphy.pic.sph_smoothing_kernels import linear_isotropic, trigonometric, gaussian, linear_tp\n",
    "\n",
    "r = np.linspace(0, 1, 100)\n",
    "out0 = np.zeros_like(r)\n",
    "x = np.linspace(-1, 1, 200)\n",
    "out1 = np.zeros_like(x)\n",
    "out2 = np.zeros_like(x)\n",
    "out3 = np.zeros_like(x)\n",
    "for i, ri in enumerate(r):\n",
    "    out0[i] = linear_isotropic(ri, 1.)\n",
    "out0b = np.zeros_like(x)\n",
    "out0b[:100] = out0[::-1]\n",
    "out0b[100:] = out0\n",
    "for i, xi in enumerate(x):\n",
    "    out1[i] = trigonometric(xi, 0., 0., 1., 1., 1.)\n",
    "    out2[i] = gaussian(xi, 0., 0., 1., 1., 1.)\n",
    "    out3[i] = linear_tp(xi, 0., 0., 1., 1., 1.)\n",
    "plt.plot(x, out0b, label=\"linear_isotropic\")\n",
    "plt.plot(x, out1, label=\"trigonometric\")\n",
    "plt.plot(x, out2, label=\"gaussian\")\n",
    "plt.plot(x, out3, label = \"linear_tp\")\n",
    "plt.title('Some smoothing kernels')\n",
    "plt.legend()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from struphy.propagators.propagators_markers import PushEta\n",
    "\n",
    "# default parameters of Propagator\n",
    "opts_eta = PushEta.options(default=False)\n",
    "print(opts_eta)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# pass simulation parameters to Propagator class\n",
    "PushEta.domain = domain"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# instantiate Propagator object\n",
    "prop_eta = PushEta(particles, algo = \"forward_euler\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from struphy.propagators.propagators_markers import PushVinSPHpressure\n",
    "\n",
    "# default parameters of Propagator\n",
    "opts_sph = PushVinSPHpressure.options(default=False)\n",
    "print(opts_sph)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# pass simulation parameters to Propagator class\n",
    "PushVinSPHpressure.domain = domain"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# instantiate Propagator object\n",
    "algo = \"forward_euler\"\n",
    "kernel_width = (h1, h2, h3)\n",
    "prop_v = PushVinSPHpressure(particles,\n",
    "                            kernel_type = kernel_type,\n",
    "                            kernel_width = kernel_width, \n",
    "                            algo = algo)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "\n",
    "# time stepping\n",
    "dt = 0.04\n",
    "Nt = 50\n",
    "Np = particles.positions.shape[0]\n",
    "\n",
    "pos = np.zeros((Nt + 1, Np, 3), dtype=float)\n",
    "velo = np.zeros((Nt + 1, Np, 3), dtype=float)\n",
    "energy = np.zeros((Nt + 1, Np), dtype=float)\n",
    "\n",
    "pos[0] = domain(particles.positions).T\n",
    "velo[0] = particles.velocities\n",
    "\n",
    "time = 0.\n",
    "time_vec = np.zeros(Nt + 1, dtype=float)\n",
    "n = 0\n",
    "\n",
    "while n < Nt:\n",
    "    time += dt\n",
    "    n += 1\n",
    "    time_vec[n] = time\n",
    "    \n",
    "    # advance in time\n",
    "    prop_eta(dt/2)\n",
    "    prop_v(dt)\n",
    "    prop_eta(dt/2)\n",
    "    \n",
    "    # positions on the physical domain Omega\n",
    "    pos[n] = domain(particles.positions).T\n",
    "    velo[n] = particles.velocities\n",
    "    \n",
    "    print(f'{n} time steps done.')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.figure(figsize=(12, 28))\n",
    "interval = Nt/10\n",
    "plot_ct = 0\n",
    "for i in range(Nt):\n",
    "    if i % interval == 0:\n",
    "        print(f'{i = }')\n",
    "        plot_ct += 1\n",
    "        plt.subplot(5, 2, plot_ct)\n",
    "        ax = plt.gca() \n",
    "        coloring = weights\n",
    "        plt.scatter(pos[i, :, 0], pos[i, :, 1], c=coloring, s=.25)\n",
    "        plt.axis('square')\n",
    "        plt.title('n0_scatter')\n",
    "        plt.xlim(l1, r1)\n",
    "        plt.ylim(l2, r2)\n",
    "        plt.colorbar()\n",
    "        plt.title(f'Gas at t={i*dt}')\n",
    "    if plot_ct == 10:\n",
    "        break"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "tenv",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.12"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
