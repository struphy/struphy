.. _api:

API
===

Struphy is primarily used via the console, by running available
models with the desired input parameters (see :ref:`quickstart`).
However, there is also an increasing number of Python modules in Struphy
that can be imported and used in other Python programs via the Struphy API:

.. toctree::
   :maxdepth: 1

   ../api/particles
   ../api/discrete_derham
   ../api/mapped_domains
   ../api/mhd_equilibria
   ../api/disp_rels

