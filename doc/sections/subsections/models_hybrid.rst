.. _hybrid_models:

Fluid-kinetic hybrid models
---------------------------

The bulk plasma is fluid, but there is also at least one kinetic species (e.g. energetic particles).

.. inheritance-diagram:: struphy.models.hybrid
    :parts: 1

.. automodule:: struphy.models.hybrid
    :members:
    :undoc-members:
    :exclude-members: propagators, scalar_quantities, update_scalar_quantities, bulk_species, velocity_scale, species, options, propagators_dct
    :show-inheritance:
    :special-members: __em_fields__, __fluid_species__, __kinetic_species__, __bulk_species__, __velocity_scale__, __propagators__