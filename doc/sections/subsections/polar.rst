.. _polar_splines:

Polar splines
-------------

Basic modules
^^^^^^^^^^^^^

.. automodule:: struphy.polar.basic
    :members: 
    :undoc-members:
    :show-inheritance:

.. _extraction_ops:

Extraction operators
^^^^^^^^^^^^^^^^^^^^

.. automodule:: struphy.polar.extraction_operators
    :members: 
    :undoc-members:
    :show-inheritance:

Linear operators
^^^^^^^^^^^^^^^^

.. automodule:: struphy.polar.linear_operators
    :members: 
    :undoc-members:
    :show-inheritance: