.. _kinetic_models:

Kinetic models
--------------

The bulk plasma is kinetic, out of thermal equilibirum; there can be fluid components too (not bulk).

.. inheritance-diagram:: struphy.models.kinetic
    :parts: 1

.. automodule:: struphy.models.kinetic
    :members:
    :undoc-members:
    :exclude-members: propagators, scalar_quantities, update_scalar_quantities, bulk_species, velocity_scale, species, options, propagators_dct
    :show-inheritance:
    :special-members: __em_fields__, __fluid_species__, __kinetic_species__, __bulk_species__, __velocity_scale__, __propagators__