.. _equils:

Fluid backgrounds
-----------------

The following inheritance diagram shows the fluid backgrounds available in Struphy:

.. inheritance-diagram:: struphy.fields_background.equils
    :parts: 1

.. toctree::
    :maxdepth: 1
    :caption: Contents:

    mhd_equils_sub





