.. _props_markers:

Particle propagators
--------------------

.. inheritance-diagram:: struphy.propagators.propagators_markers
    :parts: 1

.. automodule:: struphy.propagators.propagators_markers
    :members:
    :undoc-members:
    :exclude-members: variables, options
    :show-inheritance: