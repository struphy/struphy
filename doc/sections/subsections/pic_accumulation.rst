.. _accum_kernels:

6D accumulation kernels
-----------------------

.. automodule:: struphy.pic.accumulation.accum_kernels
    :members:
    :undoc-members:
    :exclude-members: variables
    :show-inheritance:

.. _accum_kernels_gc:

5D accumulation kernels
-----------------------

.. automodule:: struphy.pic.accumulation.accum_kernels_gc
    :members:
    :undoc-members:
    :exclude-members: variables
    :show-inheritance:

.. _filters:

Filters
-------

.. automodule:: struphy.pic.accumulation.filter_kernels
    :members:
    :undoc-members:
    :exclude-members: variables
    :show-inheritance: