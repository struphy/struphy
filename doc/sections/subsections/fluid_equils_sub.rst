.. _fluid_base:

Base classes
^^^^^^^^^^^^

.. automodule:: struphy.fields_background.fluid_equil.base
    :members:
    :undoc-members: 
    :exclude-members: 
    :show-inheritance:


.. _fluid_equil_avail:

Available fluid equilibria
^^^^^^^^^^^^^^^^^^^^^^^^^^

.. automodule:: struphy.fields_background.fluid_equil.equils
    :members:
    :undoc-members:
    :show-inheritance: