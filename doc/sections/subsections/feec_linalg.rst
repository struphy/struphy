Preconditioners
---------------

.. automodule:: struphy.feec.preconditioner
    :members:
    :undoc-members:
    :show-inheritance:

Linear operators
----------------

.. autoclass:: psydac.linalg.basic.LinearOperator
    :members:
    :undoc-members:
    :show-inheritance:

.. autoclass:: psydac.linalg.block.BlockLinearOperator
    :members:
    :undoc-members:
    :show-inheritance:

.. automodule:: struphy.feec.linear_operators
    :members:
    :undoc-members:
    :show-inheritance:

Utilities
---------

.. automodule:: struphy.feec.utilities
    :members:
    :undoc-members:
    :show-inheritance:

Variational utilities
---------------------

.. automodule:: struphy.feec.variational_utilities
    :members:
    :undoc-members:
    :show-inheritance: