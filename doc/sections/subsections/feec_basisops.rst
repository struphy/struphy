.. _basis_ops:

Basis projection operators
--------------------------

.. automodule:: struphy.feec.basis_projection_ops
    :members:
    :undoc-members:
    :show-inheritance: