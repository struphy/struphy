.. _weighted_mass:

Weighted mass operators
-----------------------

.. automodule:: struphy.feec.mass
    :members:
    :undoc-members:
    :show-inheritance:
