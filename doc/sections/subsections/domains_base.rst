Base classes
------------

.. automodule:: struphy.geometry.base
    :members:
    :special-members:
    :show-inheritance:
    :exclude-members: __init__