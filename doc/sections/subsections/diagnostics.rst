.. _diagnostics:

Diagnostic tools
----------------

Diagnostics are a vital part for interpreting simulation results.
Sometimes these tools just facilitate a proper visualization of the results.
Struphy aims to provide a wide range of diagnostic tools that have general applicability.

.. automodule:: struphy.diagnostics.diagn_tools
    :members:
    :undoc-members:

.. automodule:: struphy.diagnostics.continuous_spectra
    :members:
    :undoc-members: