.. _struphy_model:

Struphy model base class
------------------------

.. automodule:: struphy.models.base
    :members: StruphyModel
    :undoc-members:
    :show-inheritance: