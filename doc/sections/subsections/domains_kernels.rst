.. _domain_kernels:

Evaluation kernels
------------------

.. automodule:: struphy.geometry.evaluation_kernels
    :members:
    :show-inheritance:

Mapping kernels
---------------

.. automodule:: struphy.geometry.mappings_kernels
    :members:
    :show-inheritance:

Transform kernels
-----------------

.. automodule:: struphy.geometry.transform_kernels
    :members:
    :show-inheritance: