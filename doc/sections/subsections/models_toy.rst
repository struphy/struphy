.. _toy_models:

Toy models
----------

Simple toy models for testing.

.. inheritance-diagram:: struphy.models.toy
    :parts: 1

.. automodule:: struphy.models.toy
    :members:
    :undoc-members:
    :exclude-members: propagators, scalar_quantities, update_scalar_quantities, bulk_species, velocity_scale, species, options, propagators_dct
    :show-inheritance:
    :special-members: __em_fields__, __fluid_species__, __kinetic_species__, __bulk_species__, __velocity_scale__, __propagators__