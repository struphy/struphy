.. _projected_mhd:

Projected MHD equilibrium
-------------------------

.. automodule:: struphy.fields_background.projected_equils
    :members:
    :undoc-members:
    :show-inheritance:
