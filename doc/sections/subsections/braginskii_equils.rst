.. _braginskii_equil:

Ion Braginskii equilibria
-------------------------

These are analytical fluid equilibria obtained as stationary solutions of the ion Braginskii equations.

References:

* `Braginskii 1958 <http://jetp.ras.ru/cgi-bin/dn/e_006_02_0358.pdf>`_
* `Possanner, Negulescu 2016 <https://scipub.euro-fusion.org/wp-content/uploads/eurofusion/WP15ERPR1614156_accepted_merged.pdf>`_
* `Possanner et al. 2017 <https://www.sciencedirect.com/science/article/pii/S0010465517301601>`_

.. inheritance-diagram:: struphy.fields_background.braginskii_equil.equils
    :parts: 1

.. toctree::
    :maxdepth: 2
    :caption: Contents:

    braginskii_equils_sub






