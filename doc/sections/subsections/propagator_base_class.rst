.. _prop_base:

Propagator base class
---------------------

.. automodule:: struphy.propagators.base
    :members:
    :undoc-members:
    :show-inheritance: