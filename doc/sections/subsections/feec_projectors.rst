.. _projectors:

Projections into Derham
-----------------------

.. automodule:: struphy.feec.projectors
    :members:
    :undoc-members:
    :show-inheritance:

.. autoclass:: psydac.feec.global_projectors.GlobalProjector
    :members:
    :undoc-members:
    :show-inheritance: