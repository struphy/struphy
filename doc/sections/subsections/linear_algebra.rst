.. _linalg:

Linear algebra
--------------

.. automodule:: struphy.linear_algebra.schur_solver
    :members: 
    :undoc-members:

.. automodule:: struphy.linear_algebra.linalg_kernels
    :members: 
    :undoc-members:

.. automodule:: struphy.linear_algebra.linalg_kron
    :members: 
    :undoc-members:


