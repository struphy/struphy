.. _braginskii_base:

Base class
^^^^^^^^^^

.. automodule:: struphy.fields_background.braginskii_equil.base
    :members:
    :undoc-members: 
    :exclude-members: 
    :show-inheritance:


.. _braginskii_equil_avail:

Available Braginskii equilibria
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. automodule:: struphy.fields_background.braginskii_equil.equils
    :members:
    :undoc-members:
    :show-inheritance: