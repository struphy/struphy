.. _pic_utilities:

Utilities 
---------

.. automodule:: struphy.pic.sampling_kernels
    :members:
    :undoc-members:
    :show-inheritance:

.. automodule:: struphy.pic.sobol_seq
    :members:
    :undoc-members:
    :show-inheritance:

.. automodule:: struphy.pic.utilities
    :members:
    :undoc-members:
    :show-inheritance: