.. _pic_base:

Base modules 
------------

.. inheritance-diagram:: struphy.pic.particles
    :parts: 1

.. automodule:: struphy.pic.base
    :members:
    :undoc-members:
    :show-inheritance:

.. automodule:: struphy.pic.particles
    :members:
    :undoc-members:
    :show-inheritance:

.. _pushers:

Pusher modules
--------------

.. autoclass:: struphy.pic.pushing.pusher.Pusher
    :members:
    :undoc-members:
    :exclude-members: variables
    :show-inheritance:

.. autoclass:: struphy.pic.pushing.pusher.ButcherTableau
    :members:
    :undoc-members:
    :exclude-members: available_methods
    :show-inheritance:
    :special-members: __available_methods__

.. _accumulator:

Particle-to-grid coupling
-------------------------

.. automodule:: struphy.pic.accumulation.particles_to_grid
    :members:
    :undoc-members:
    :exclude-members: variables
    :show-inheritance: