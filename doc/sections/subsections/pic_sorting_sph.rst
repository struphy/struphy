.. _pic_utilities:

Sorting
-------

.. automodule:: struphy.pic.sorting_kernels
    :members:
    :undoc-members:
    :show-inheritance:

SPH evaluation
--------------

.. automodule:: struphy.pic.sph_eval_kernels
    :members:
    :undoc-members:
    :show-inheritance:

Smoothing kernels
-----------------

.. automodule:: struphy.pic.sph_smoothing_kernels
    :members:
    :undoc-members:
    :show-inheritance: