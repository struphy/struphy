.. _domains_avail:

Available domains
-----------------

.. inheritance-diagram:: struphy.geometry.domains
    :parts: 1

.. automodule:: struphy.geometry.domains
    :members:
    :exclude-members: kind_map, params_map, params_numpy, pole, periodic_eta3
    :show-inheritance:
