.. _fluid_equil:

Fluid equilibria
----------------

The following inheritance diagram shows the fluid equilibria available in Struphy:

.. inheritance-diagram:: struphy.fields_background.fluid_equil.equils
    :parts: 1

.. toctree::
    :maxdepth: 2
    :caption: Contents:

    fluid_equils_sub