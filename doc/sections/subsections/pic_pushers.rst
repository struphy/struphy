Pusher kernels
--------------

.. automodule:: struphy.pic.pushing.pusher_kernels
    :members:
    :undoc-members:
    :exclude-members: variables
    :show-inheritance:

Pusher kernels guiding-center
-----------------------------

.. automodule:: struphy.pic.pushing.pusher_kernels_gc
    :members:
    :undoc-members:
    :exclude-members: variables
    :show-inheritance:

Evaluation kernels guiding-center
---------------------------------

.. automodule:: struphy.pic.pushing.eval_kernels_gc
    :members:
    :undoc-members:
    :exclude-members: variables
    :show-inheritance: