Post processing
---------------

General
^^^^^^^

.. automodule:: struphy.post_processing.post_processing_tools
    :members: 
    :undoc-members:
    :show-inheritance:


Particle orbits
^^^^^^^^^^^^^^^

.. automodule:: struphy.post_processing.orbits.orbits_tools
    :members: 
    :undoc-members:
    :show-inheritance:

.. automodule:: struphy.post_processing.orbits.orbits_kernels
    :members: 
    :undoc-members:
    :show-inheritance:


Profiling
^^^^^^^^^

.. automodule:: struphy.post_processing.cprofile_analyser
    :members: 
    :undoc-members:
    :show-inheritance: