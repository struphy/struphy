.. _avail_inits:

Perturbations
-------------

Perturbations can be added on top of :ref:`equils_avail` or :ref:`kinetic_backgrounds`.
Below are examples for dictionary structures to be put under the ``perturbations``
key of a ``.yml`` parameter file.

.. toctree::
    :maxdepth: 2
    :caption: Contents:

    inits_sub