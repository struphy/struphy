.. _de_rham:

Derham sequence (3D) 
--------------------

.. automodule:: struphy.feec.psydac_derham
    :members:
    :undoc-members:
    :show-inheritance:

.. autoclass:: psydac.fem.tensor.TensorFemSpace
    :members:
    :undoc-members:
    :show-inheritance:

.. autoclass:: psydac.fem.vector.VectorFemSpace
    :members:
    :undoc-members:
    :show-inheritance:    