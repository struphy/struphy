Input/output
------------

Setup modules
^^^^^^^^^^^^^

.. automodule:: struphy.io.setup
    :members: 
    :undoc-members:

Data modules
^^^^^^^^^^^^

.. automodule:: struphy.io.output_handling
    :members: 
    :undoc-members: