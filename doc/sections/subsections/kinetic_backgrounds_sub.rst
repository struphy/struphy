Available Maxwellians
^^^^^^^^^^^^^^^^^^^^^
    
.. automodule:: struphy.kinetic_background.maxwellians
    :members:
    :undoc-members: 
    :show-inheritance:


.. _moment_functions:

Moment functions
^^^^^^^^^^^^^^^^

.. automodule:: struphy.kinetic_background.moment_functions
    :members:
    :undoc-members: 
    :show-inheritance:


Base classes
^^^^^^^^^^^^

.. automodule:: struphy.kinetic_background.base
    :members:
    :undoc-members: 
    :show-inheritance: