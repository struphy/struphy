.. _props_coupling:

Particle-field coupling propagators
-----------------------------------

.. inheritance-diagram:: struphy.propagators.propagators_coupling
    :parts: 1

.. automodule:: struphy.propagators.propagators_coupling
    :members:
    :undoc-members:
    :exclude-members: variables, options
    :show-inheritance: