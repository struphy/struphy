.. _field_props:

Field propagators
-----------------

.. inheritance-diagram:: struphy.propagators.propagators_fields
    :parts: 1

.. automodule:: struphy.propagators.propagators_fields
    :members:
    :undoc-members:
    :exclude-members: variables, options
    :show-inheritance: