B-splines
---------

Basic modules
^^^^^^^^^^^^^

.. automodule:: struphy.bsplines.bsplines
    :members: 
    :undoc-members:

.. automodule:: struphy.bsplines.bsplines_kernels
    :members: 
    :undoc-members:

Spline evaluation
^^^^^^^^^^^^^^^^^

.. automodule:: struphy.bsplines.evaluation_kernels_3d
    :members: 
    :undoc-members:

.. automodule:: struphy.bsplines.evaluation_kernels_2d
    :members: 
    :undoc-members:

.. automodule:: struphy.bsplines.evaluation_kernels_1d
    :members: 
    :undoc-members:

Shape functions
^^^^^^^^^^^^^^^

.. automodule:: struphy.bsplines.shapefunc_kernels
    :members: 
    :undoc-members: