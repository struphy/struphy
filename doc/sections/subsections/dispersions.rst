.. _disp_rels:

Dispersion relations
--------------------

Dispersion relations are a valuable tool for code verification.


Base classes
^^^^^^^^^^^^

.. automodule:: struphy.dispersion_relations.base
    :members:
    :undoc-members:
    :show-inheritance:


Available dispersion relations
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. inheritance-diagram:: struphy.dispersion_relations.analytic
    :parts: 1

.. automodule:: struphy.dispersion_relations.analytic
    :members:
    :undoc-members:
    :show-inheritance: