.. _kinetic_backgrounds:

Kinetic backgrounds
-------------------

Kinetic backgrounds are often thermal equilibria like Maxwellian distributions.

.. inheritance-diagram:: struphy.kinetic_background.maxwellians
    :parts: 1

.. toctree::
    :maxdepth: 1
    :caption: Contents:

    kinetic_backgrounds_sub

