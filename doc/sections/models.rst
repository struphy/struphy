.. _models:

Models
======

Struphy features a variety of plasma models for different physics scenarios.
All models are implemented with a specific :ref:`normalization`. 
A complete list of the currently available models can be called from the console via::

    struphy run -h

To add a new model, please visit :ref:`add_model`.

.. toctree::
    :maxdepth: 1
    :caption: Contents:

    subsections/normalization
    subsections/models_fluid
    subsections/models_kinetic
    subsections/models_hybrid
    subsections/models_toy
    subsections/model_base_class
   

















