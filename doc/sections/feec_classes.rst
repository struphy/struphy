.. _Tutorial 3 - De Rham: ../tutorials/tutorial_03_discrete_derham.ipynb
.. _Tutorial 6 - Poisson: ../tutorials/tutorial_06_poisson.ipynb
.. _Tutorial 7 - Heat equation: ../tutorials/tutorial_07_heat_equation.ipynb
.. _Tutorial 8 - Maxwell equations: ../tutorials/tutorial_08_maxwell.ipynb
.. _Tutorial 10 - Linear MHD equations: ../tutorials/tutorial_10_linear_mhd.ipynb


.. _feec_base:

FEEC modules
------------

Check out the following tutorials for how to use the FEEC modules below:

* `Tutorial 3 - De Rham`_
* `Tutorial 6 - Poisson`_
* `Tutorial 7 - Heat equation`_
* `Tutorial 8 - Maxwell equations`_
* `Tutorial 10 - Linear MHD equations`_

.. toctree::
    :maxdepth: 1
    :caption: Contents:

    subsections/feec_derham
    subsections/feec_projectors
    subsections/feec_weightedmass
    subsections/feec_basisops
    subsections/feec_projected_mhd
    subsections/feec_linalg


