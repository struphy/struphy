.. _Tutorial 1 - Kinetic particles: ../tutorials/tutorial_01_kinetic_particles.ipynb
.. _Tutorial 2 - Fluid particles: ../tutorials/tutorial_02_fluid_particles.ipynb
.. _Tutorial 9 - Vlasov-Maxwell: ../tutorials/tutorial_09_vlasov_maxwell.ipynb

.. _pic_modules:

Particle modules
================

Check out the following tutorials for how to use the particle modules below:

* `Tutorial 1 - Kinetic particles`_
* `Tutorial 2 - Fluid particles`_
* `Tutorial 9 - Vlasov-Maxwell`_

.. toctree::
    :maxdepth: 1
    :caption: Contents:

    subsections/pic_base
    subsections/pic_pushers
    subsections/pic_accumulation
    subsections/pic_sorting_sph
    subsections/pic_utilities







