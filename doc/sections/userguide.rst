.. _userguide:

Userguide
=========

There are two basic modes of how Struphy can be used:

1. in Python programs, as for instance described in the notebook :ref:`tutorials`
2. via the Struphy CLI (command line interface)

In the CLI the overall help is displayed by typing::

    struphy -h

To get more information on the sub-commands::

    struphy COMMAND -h

The installed version is obtained by::

    struphy -v

.. toctree::
    :maxdepth: 1
    :caption: Contents:

    subsections/struphy_cli
    subsections/parameters
    subsections/initial_conditions
    subsections/boundary_conditions
    subsections/profiling
    subsections/post_processing
    subsections/paraview








   







