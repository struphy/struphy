.. _tutorials:

Tutorials
=========

All notebooks are available at https://gitlab.mpcdf.mpg.de/struphy/struphy/-/blob/devel/doc/tutorials/.

The objects used in these notebooks are the same as in the available :ref:`models`.
They can thus be used for MPI parallel runs in HPC applications.

.. toctree::
   :maxdepth: 1
   :caption: Notebook tutorials:

   ../tutorials/tutorial_01_kinetic_particles
   ../tutorials/tutorial_02_fluid_particles
   ../tutorials/tutorial_03_discrete_derham
   ../tutorials/tutorial_04_mapped_domains
   ../tutorials/tutorial_05_mhd_equilibria
   ../tutorials/tutorial_06_poisson
   ../tutorials/tutorial_07_heat_equation
   ../tutorials/tutorial_08_maxwell
   ../tutorials/tutorial_09_vlasov_maxwell
   ../tutorials/tutorial_10_linear_mhd
   ../tutorials/tutorial_11_data_structures
   ../tutorials/tutorial_12_struphy_data_pproc
