.. _gallery:

Struphy examples
================

Check out some result produced by Struphy simulations.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   subsections/performance_tests