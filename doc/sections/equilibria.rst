.. _equilibria:

Equilibria
==========

Fluid/kinetic equilibria (or backgrounds) are often the starting point of dynamical plasma simulations.
In Struphy they can be used for setting :ref:`initial_conditions`, or for providing the background in
":math:`\delta f`-models", where solutions are computed w.r.t a given background.

.. toctree::
    :maxdepth: 1
    :caption: Contents:

    subsections/mhd_equils
    subsections/kinetic_backgrounds
