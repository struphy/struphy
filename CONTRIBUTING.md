# Contact

* [RocketChat developer's channel](https://chat.gwdg.de/channel/struphy-developers)
* [Issue tracker](https://gitlab.mpcdf.mpg.de/struphy/struphy/-/issues) (MPCDF account needed, contacts below)
* [LinkedIn](https://www.linkedin.com/company/struphy/)
* [stefan.possanner@ipp.mpg.de](mailto:spossann@ipp.mpg.de)
* [eric.sonnendruecker@ipp.mpg.de](mailto:eric.sonnendruecker@ipp.mpg.de)
* [xin.wang@ipp.mpg.de](mailto:xin.wang@ipp.mpg.de)


# Repository

Struphy has two main branches, **master** and **devel**. 
Nobody can push directly to these branches.

The **master** branch holds the current release of the code. 

**devel** is the main branch for developers. Feature branches must be checked out and merged into **devel**.


# Forking

In case you are not a [member](https://gitlab.mpcdf.mpg.de/struphy/struphy/-/project_members>) of the Struphy project,
you can contribute code by [forking](https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html>)the Struphy
repository.

You must create a **public fork** to be able to merge your code into Struphy!

You can create feature branches in your forked repo and create merge requests into the original Struphy repo.

[Update your fork](https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html#from-the-command-line)
in case **devel** changes in the Struphy repo while you are working on your feature.

# More info

See the [Developer's guide](https://struphy.pages.mpcdf.de/struphy/sections/developers.html).