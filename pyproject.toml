[build-system]
requires = ["setuptools>=61.0"]
build-backend = "setuptools.build_meta"

[project]
name = "struphy"
version = "2.4.0"
readme = "README.md"
requires-python = ">=3.10"
license = {file = "LICENSE"}
authors = [
  { name = "Max Planck Institute for Plasma Physics"},
  { email = "stefan.possanner@ipp.mpg.de"},
  { email = "eric.sonnendruecker@ipp.mpg.de"}
]
description = "Multi-model plasma physics package"
keywords = ["plasma physics, fusion, numerical modeling, partial differential equations, energetic particles"]
classifiers = [
    "Programming Language :: Python :: 3",
    ]
dependencies = [
    'numpy',
    'pyccel>=1.11.1',
    'mpi4py<4',
    'scipy',
    'h5py',
    'matplotlib',
    'pyyaml',
    'psydac @ git+https://github.com/max-models/psydac-for-struphy.git@devel',
    'vtk',
    'tqdm',
    'argcomplete',
    'gvec-to-python',
    'desc-opt',
    'netcdf4<1.7',
]

[project.optional-dependencies]
test = [
    'pytest',
    'pytest-mpi',
]
dev = [
    'struphy[test]',
    'notebook',
    'autopep8',
    'isort',
    'flake8',
    'pylint',
    'add-trailing-comma',
    'ruff>=0.9.1',
    'pre-commit',
]
doc = [
    'sphinx',
    'sphinx-design',
    'lxml_html_clean',
    'sphinxcontrib-napoleon',
    'pydata-sphinx-theme',
    'nbsphinx',
    'm2r2',
    'myst-parser',
    'mistune',
    'docutils',
    'ipyparallel',
]


[project.urls]
homepage = "https://struphy.pages.mpcdf.de/struphy/"
documentation = "https://struphy.pages.mpcdf.de/struphy/"
repository = "https://gitlab.mpcdf.mpg.de/struphy/struphy"
changelog = "https://gitlab.mpcdf.mpg.de/struphy/struphy/-/blob/devel/CHANGELOG.md"
"Bug Tracker" = "https://gitlab.mpcdf.mpg.de/struphy/struphy/-/issues"

[project.scripts]
struphy = "struphy.console.main:struphy"

[tool.setuptools.package-data]
'struphy.fields_background.mhd_equil.eqdsk' = [
            'data/*.high',
        ]

'struphy.fields_background.mhd_equil.gvec' = [
            'ellipstell_v2/newBC_E1D6_M6N6/*.dat',
            'ellipstell_v2/newBC_E1D6_M6N6/*.ini',
            'ellipstell_v2/newBC_E4D6_M6N6/*.dat',
            'ellipstell_v2/newBC_E4D6_M6N6/*.ini',
            'ellipstell_v2/oldBC_E40D5M6N6/*.dat',
            'ellipstell_v2/oldBC_E40D5M6N6/*.ini',
        ]

'struphy.io' = ['batch/*.sh']

'struphy.io.inp' = ['parameters.yml',
                    'tests/*.yml',
                    'tutorials/*.yml',
                    'longer_examples/*.yml',
                    'verification/*.yml',
                    ]
                    
struphy = ['compile_struphy.mk',
           'psydac-2.4.0-py3-none-any.whl',]

[tool.autopep8]
# Add specific settings for autopep8
max_line_length = 120
ignore = ["E221,E226,E203,E265,E731,E711"]
# Descriptions of error codes: https://www.flake8rules.com/
# E201 – Whitespace after  (, [ or {
# E202 – Whitespace before (, [ or {
# E221 – Multiple spaces before operator
# E226 – Missing whitespace around arithmetic operators
# E265 - Block comment should start with '# '
# E266 – Too many leading # for block comment
# E731 – Do not assign a lambda expression
# E203 – Whitespace before : (used in slicing)
# E711 - Ignore enforcing 'is not None' over '!= None'

[tool.isort]
# Configuration for isort
profile = "black"  # Using the Black profile to match formatting styles
line_length = 120
multi_line_output = 3  # Control how isort wraps imports
include_trailing_comma = true
force_grid_wrap = 0
combine_as_imports = true

[tool.pylint]
max-line-length=120
quote-style = "single"

[tool.ruff]
line-length=120
lint.ignore = [
    "E265",  # Checks for block comments that lack a single space after the leading # character.
    "E402",  # Module-level imports should be placed at the top of the file (before any other code)
    "E701",  # Multiple statements on one line (colon)
    # "E711",  # Comparison to `None` should be `cond is None`
    # "E712",  # Comparison to True, False, or None should be avoided (e.g., use `if variable:` instead of `if variable == True:`)
    # "E713",  # Convert to `not in`
    # "E721",  # Avoid comparing types directly with `type()` (e.g., use `isinstance()` instead of `type(variable) == list`)
    "E722",  # Do not use bare 'except'
    "E731",  # Do not assign a lambda expression, use a def
    "E741",  # Do not use variables named 'l', 'O', or 'I'
    "E742",  # Do not define classes named 'l', 'O', or 'I'
    "E743",  # Do not define functions named 'l', 'O', or 'I'
    "E902",  # IOError
    # "E999",  # SyntaxError
    "F401",  # Module imported but unused
    "F403",  # 'from module import *' used; unable to detect undefined names
    "F405",  # Name may be undefined, or defined from star imports: module
    # "F811",  # Redefinition of a variable that might cause ambiguity (e.g., reusing a variable name in a conflicting scope)
    # "F821",  # Undefined name
    # "F822",  # Undefined name in __all__
    # "F823",  # Local variable name referenced before assignment
    # "F841",  # Local variable name assigned to but never used
    "D211",  # Checks for docstrings on class definitions that are preceded by a blank line
    "D213",  # Checks for docstring summary lines that are not positioned on the second physical line of the docstring
]
